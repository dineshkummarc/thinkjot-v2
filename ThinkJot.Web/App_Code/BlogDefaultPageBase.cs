/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ThinkJot.Core.Blogs;
using ThinkJot.Core.Utils;
using System.Collections.Generic;

/// <summary>
/// Summary description for DefaultBlogPageHandler
/// </summary>
public abstract class BlogDefaultPageBase : BlogPageBase, IBlogEntryList
{
    public event BlogEntriesDisplayedDelegate BlogEntriesDisplayed;

    public BlogDefaultPageBase()
    {
        this.Load += new EventHandler(Page_Load);
    }

    private void Page_Load(object sender, EventArgs args)
    {
        Init();
    }  

    new private void Init()
    {
        PlaceHolder mainPanel = GetMainPanel();

        if (new SecurityUtility().IsBlogEditor(BlogName))
        {
            BlogControlBase adminBar = (BlogControlBase)Page.LoadControl("~/Blogs/AdminBar.ascx");
            mainPanel.Controls.Add(adminBar);
        }

        string function = Request["function"];
        BlogControlBase control;
        switch (function)
        {
            case "Search":
                control = (BlogControlBase)Page.LoadControl("~/Blogs/SearchResultsView.ascx");
                break;
            default:
                control = (BlogControlBase)Page.LoadControl("~/Blogs/BlogEntriesView.ascx");

                //If the control supports Load complete notification, then attach a handler to process it.
                IBlogEntryList entryList = control as IBlogEntryList;
                if (entryList != null)
                    entryList.BlogEntriesDisplayed += new BlogEntriesDisplayedDelegate(entryList_BlogEntriesDisplayed);
                break;
        }
        mainPanel.Controls.Add(control);
    }
    public abstract PlaceHolder GetMainPanel();

    private void entryList_BlogEntriesDisplayed(List<BlogEntry> entries)
    {
        if (BlogEntriesDisplayed != null)
            BlogEntriesDisplayed(entries);
    }
}
