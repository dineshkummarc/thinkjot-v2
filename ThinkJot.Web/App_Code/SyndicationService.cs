/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Xml;

/// <summary>
/// Summary description for SyndicationService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SyndicationService : System.Web.Services.WebService
{

    public SyndicationService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public XmlDocument GetRss()
    {
        XmlDocument doc = new XmlDocument();
        string rssPath = HttpContext.Current.Request.MapPath("rss.xml");
        doc.Load(rssPath);
        return doc;
    }
}

