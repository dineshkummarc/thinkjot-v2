/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Security;
using System.Collections.Generic;

using ThinkJot.Core;
using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs;

/// <summary>
/// Summary description for BlogService
/// </summary>
[WebService(Namespace = "http://www.process64.com/thinkjot")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class BlogService : System.Web.Services.WebService
{
    public BlogService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class WSError
    {
        public long Code = 0;
        public string Description = "";
    }

    public class WSResult
    {
        public WSError Error = new WSError();

        public void SetError(ErrorInfo error)
        {
            Error.Code = error.ErrorCode;
            Error.Description = error.Message;
        }
    }

    /* Is Logged In? */
    public class IsLoggedInResult : WSResult
    {
        public bool IsLoggedIn = false;
    }

    [WebMethod(EnableSession = true)]
    public IsLoggedInResult IsLoggedIn()
    {
        IsLoggedInResult result = new IsLoggedInResult();
        result.IsLoggedIn = new SecurityUtility().IsLoggedIn();
        return result;
    }
    /* is logged in! */

    [WebMethod(EnableSession = true)]
    public void DeleteBlogEntry(Guid entryId, string blogName)
    {
        //Editors can delete entries
        if (new SecurityUtility().IsBlogEditor(blogName))
        {
            BlogEntry.DeleteEntry(entryId, blogName);       
        }
    }

    [WebMethod(EnableSession = true)]
    public void DeleteComment(Guid entryId, Guid commentId, string blogName)
    {
        //Editors can delete entries
        if (new SecurityUtility().IsBlogEditor(blogName))
        {
            Comment.DeleteComment(entryId, commentId, blogName);
        }
    }

    [WebMethod(EnableSession = true)]
    public void AddCategory(string categoryName, string categoryDescription, string blogName)
    {
        if (string.IsNullOrEmpty(categoryName)) return;
        //Editors can add categories
        if (!new SecurityUtility().IsBlogEditor(blogName)) return;
        Category category = new Category();
        category.Name = categoryName;
        category.Description = categoryDescription;
        Category.AddCategory(category, blogName);
    }
}