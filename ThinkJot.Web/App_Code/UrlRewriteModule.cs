/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.UrlMapping;

public class UrlRewriteModule : System.Web.IHttpModule 
{ 
    public void Dispose()
    {  
    }

    public void Init(HttpApplication context)
    {
        context.BeginRequest += new System.EventHandler(Rewrite_BeginRequest);
    }

    public void Rewrite_BeginRequest(object sender, System.EventArgs args)
    {
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string newUrl = UrlMappingUtility.GetMatchingRewrite(path);
        if (!string.IsNullOrEmpty(newUrl)) HttpContext.Current.RewritePath("~" + newUrl);
    }   
}
