/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
public class RedirectRule
{
    string url = "";
    public string URL
    {
        get { return url; }
        set { url = value; }
    }

    string rewriteURL = "";
    public string RewriteURL
    {
        get { return rewriteURL; }
        set { rewriteURL = value; }
    }

    string name = "";
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
}