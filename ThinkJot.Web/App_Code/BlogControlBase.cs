/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;
/// <summary>
/// Summary description for BlogControlBase
/// </summary>
public abstract class BlogControlBase : System.Web.UI.UserControl
{ 
    public BlogControlBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string BlogName
    {
        get
        {
            return ((BlogPageBase)Page).BlogName;
        }
    }

    public BlogConfig Config
    {
        get
        {
            return ((BlogPageBase)Page).Config;
        }
    }

    SystemConfiguration sysConfig = SystemConfiguration.GetConfig();
    public SystemConfiguration SysConfig
    {
        get { return sysConfig; }
    }

    PathUtility pathUtility = new PathUtility();
    public PathUtility PathUtility
    {
        get { return pathUtility; }
    }

    protected string GetBlogUrl()
    {
        return PathUtility.GetBlogUrl(BlogName);
    }

    protected string GetMappedBlogUrl()
    {
        return PathUtility.GetMappedBlogUrl(BlogName);
    }
    
    protected string GetUrlFor(string relUrl)
    {
        return PathUtility.CombineUrls(GetBlogUrl(), relUrl);
    }

    protected string GetMappedUrlFor(string relUrl)
    {
        return PathUtility.CombineUrls(GetMappedBlogUrl(), relUrl);
    }

    protected BlogControlBase GetOverridableControl(string controlName)
    {
        string controlVPath = "~/blogs/" + controlName;

        /* Control can be "overridden" in the Blog Folder :)
         * Or this means that if a control by the name controlName (say 'CommentEntryControl.ascx)
         * exists in the ~/Blogs/<blog_name> folder, we wil load it instead.         * 
         */

        string overridden = "~/blogs/" + BlogName + "/" + controlName;
        if (File.Exists(Server.MapPath(overridden))) controlVPath = overridden;
        return (BlogControlBase)Page.LoadControl(controlVPath);
    }
}
