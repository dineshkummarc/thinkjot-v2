/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers;

/// <summary>
/// Summary description for BlogPageBase
/// </summary>
public class BlogPageBase : BlogSystemPageBase
{
    private string blogName;
    public string BlogName
    {
        get { return blogName; }
    }

    private BlogConfig config;
    public BlogConfig Config
    {
        get { return config; }
    }
    
    public BlogPageBase()
    {
        this.Load += new EventHandler(Page_Load);
    }

    private void Page_Load(object sender, EventArgs args)
    {
        Init();
        if (config.EnableActivityTracking)
            LogRequestEvent();
    }

    new private void Init()
    {
        string vPath = Request.Path;
        int prefixLen = Request.ApplicationPath.TrimEnd('/').Length + "/blogs/".Length;
        if (vPath.Length > prefixLen)
        {
            int nextSlash = vPath.IndexOf('/', prefixLen);
            blogName = vPath.Substring(prefixLen, nextSlash - prefixLen);
            blogName = blogName.ToLower();
        }
        config = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
        this.Title = config.BlogTitle;
    }

    protected string GetBlogUrl()
    {
        return PathUtility.GetBlogUrl(BlogName);
    }

    protected string GetMappedBlogUrl()
    {
        return PathUtility.GetMappedBlogUrl(BlogName);
    }

    protected string GetUrlFor(string relUrl)
    {
        return PathUtility.CombineUrls(GetBlogUrl(), relUrl);
    }

    protected string GetMappedUrlFor(string relUrl)
    {
        return PathUtility.CombineUrls(GetMappedBlogUrl(), relUrl);
    }

    protected string GetBlogTitle()
    {
        return config.BlogTitle;
    }

    protected string GetRssLink()
    {
        return "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"" +
            GetBlogTitle() + "\" href=\"" + GetUrlFor("rss.xml") + "\" />";
    }

    protected string GetRsdLink()
    {
        return "<link rel=\"EditURI\" type=\"application/rsd+xml\" title=\"RSD\"" +
        " href=\"" + GetUrlFor("getrsd.aspx?blogname=" + BlogName) + "\" />";
    }

    private void LogRequestEvent()
    {
        ActivityLogEntry logEntry = new ActivityLogEntry();
        logEntry.RawUrl = Request.RawUrl;
        logEntry.RequestUrl = Request.Url.AbsoluteUri != null ? Request.Url.AbsoluteUri : "";
        logEntry.Referrer = Request.UrlReferrer != null ? Request.UrlReferrer.AbsoluteUri : "";
        logEntry.UserHostAddress = Request.UserHostAddress;
        logEntry.UserHostName = Request.UserHostName;
        logEntry.UserAgent = Request.UserAgent;
        ActivityLog.WriteToLog(logEntry, blogName);
    }
}