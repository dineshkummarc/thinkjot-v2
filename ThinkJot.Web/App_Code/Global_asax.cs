using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BlogProviders = ThinkJot.Core.Blogs.Providers;
/// <summary>
/// Summary description for Global_asax
/// </summary>
public class Global_asax
{
    public Global_asax()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void OnApplicationStart()
    {
        BlogProviders.Logger blogLogger = BlogProviders.BlogDataProvider.GetLogger();
        blogLogger.WriteToSystemLog(Guid.NewGuid(), "ThinkJot Started.");
    }

    public static void OnApplicationEnd()
    {
        BlogProviders.Logger blogLogger = BlogProviders.BlogDataProvider.GetLogger();
        blogLogger.WriteToSystemLog(Guid.NewGuid(), "ThinkJot Stopped.");
    }
}
