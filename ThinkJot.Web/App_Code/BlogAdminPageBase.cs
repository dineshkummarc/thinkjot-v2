/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Utils;

/// <summary>
/// Summary description for BlogAdminPageBase
/// </summary>
/// 
public enum AuthRequired
{
    Everyone = 10,
    LoggedInUser = 20,
    BlogEditor = 30,
    BlogAdmin = 40,
    SystemAdmin = 50
}

public abstract class BlogAdminPageBase : BlogPageBase
{
    bool mono_fix_isLoadCalled = false;

    public BlogAdminPageBase()
    {
        this.Load += new EventHandler(Page_Load);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mono_fix_isLoadCalled) return;
        mono_fix_isLoadCalled = true;

        //This page should not be accessible if the user is not logged in.
        if (!new SecurityUtility().IsLoggedIn()) return;

        Panel mainPanel = GetMainPanel();
        string function = Request["function"];
        Control control;
        switch (function)
        {
            case "ControlPanel":
                control = CheckPermissionsAndLoadControl("~/Blogs/ControlPanel.ascx", AuthRequired.BlogAdmin);
                break;
            case "AddEntry":
                control = CheckPermissionsAndLoadControl("~/Blogs/EditEntryView.ascx", AuthRequired.BlogEditor);
                break;
            case "EditEntry":
                control = CheckPermissionsAndLoadControl("~/Blogs/EditEntryView.ascx", AuthRequired.BlogEditor);
                break;
            case "ManagePosts":
                control = CheckPermissionsAndLoadControl("~/Blogs/ManagePostsView.ascx", AuthRequired.BlogAdmin);
                break;
            case "ManageFiles":
                control = CheckPermissionsAndLoadControl("~/Blogs/ManageFilesView.ascx", AuthRequired.BlogAdmin);
                break;
            case "DirectTextEdit":
                control = CheckPermissionsAndLoadControl("~/Blogs/DirectTextEditControl.ascx", AuthRequired.BlogAdmin);
                break;
            case "ReloadBlogConfig":
                ReloadBlogConfig();
                return; //RETURNS!
            case "ManageTrash":
                control = CheckPermissionsAndLoadControl("~/Blogs/ManageTrashView.ascx", AuthRequired.BlogAdmin);
                break;
            case "MigrateDasBlog":
                control = CheckPermissionsAndLoadControl("~/Blogs/MigrateDasBlog.ascx", AuthRequired.BlogAdmin);
                break;
            case "ViewReferrers":
                control = CheckPermissionsAndLoadControl("~/Blogs/ViewReferrersControl.ascx", AuthRequired.BlogAdmin);
                break;
            case "ViewSearchEngineActivity":
                control = CheckPermissionsAndLoadControl("~/Blogs/ViewSearchEngineActivityControl.ascx", AuthRequired.BlogAdmin);
                break;
            case "ViewTopPagesAndUserAgents":
                control = CheckPermissionsAndLoadControl("~/Blogs/ViewTopPagesAndUserAgents.ascx", AuthRequired.BlogAdmin);
                break;
            case "ViewVisitors":
                control = CheckPermissionsAndLoadControl("~/Blogs/ViewVisitorsControl.ascx", AuthRequired.BlogAdmin);
                break;
            case "ViewEvents":
                control = CheckPermissionsAndLoadControl("~/Blogs/ViewEventsControl.ascx", AuthRequired.BlogAdmin);
                break;
            case "BlogConfigCommon":
                control = CheckPermissionsAndLoadControl("~/Blogs/BlogConfigEditor.ascx", AuthRequired.BlogAdmin);
                break;
            case "UserManagement":
                control = CheckPermissionsAndLoadControl("~/Blogs/UserManagement.ascx", AuthRequired.SystemAdmin);
                break;
            default:
                control = LoadControl("~/Blogs/BlogEntriesView.ascx");
                break;
        }
        mainPanel.Controls.Add(control);
    }

    public abstract Panel GetMainPanel();

    private void ReloadBlogConfig()
    {
        if (new SecurityUtility().IsBlogAdministrator(BlogName))
        {
            SysConfig.Blogs.ReloadBlogConfig(BlogName);            
            GetMainPanel().Controls.Add(GetSimpleTextDisplayControl("Blog Config for " + BlogName + " was reloaded. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>"));
        }
        else
        {            
            GetMainPanel().Controls.Add(GetSimpleTextDisplayControl("Only Blog Administrators can perform this function. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>"));
        }
    }

    private BlogControlBase CheckPermissionsAndLoadControl(string controlName, AuthRequired authreq)
    {
        if (authreq == AuthRequired.SystemAdmin && !new SecurityUtility().IsSystemAdministrator())
            return GetSimpleTextDisplayControl("Only System Administrators can perform this function. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>");

        if (authreq == AuthRequired.BlogAdmin && !new SecurityUtility().IsBlogAdministrator(BlogName))
            return GetSimpleTextDisplayControl("Only Blog Administrators can perform this function. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>");

        if (authreq == AuthRequired.BlogEditor && !new SecurityUtility().IsBlogEditor(BlogName))
            return GetSimpleTextDisplayControl("Only Blog Editors can perform this function. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>");

        if (authreq == AuthRequired.LoggedInUser && !new SecurityUtility().IsLoggedIn())
            return GetSimpleTextDisplayControl("Only logged in users can perform this function. <a href=\"" + GetMappedBlogUrl() + "\">Go to blog</a>");

        return (BlogControlBase)Page.LoadControl(controlName);
    }

    private BlogControlBase GetSimpleTextDisplayControl(string textToBeLoaded)
    {
        BlogControlBase control = (BlogControlBase)Page.LoadControl("~/Blogs/SimpleTextDisplayControl.ascx");
        ((ISimpleTextDisplayControl)control).LoadText(textToBeLoaded);
        return control;           
    }
}
