/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Utils;

/// <summary>
/// Summary description for BlogPageBase
/// </summary>
public class BlogSystemPageBase : WebPageBase
{
    SystemConfiguration sysConfig = SystemConfiguration.GetConfig();
    public SystemConfiguration SysConfig
    {
        get { return sysConfig; }
    }

    PathUtility pathUtility = new PathUtility();
    public PathUtility PathUtility
    {
        get { return pathUtility; }
    }

    protected string GetCssLink(string fullPath)
    {
        return "<link media=\"all\" rel=\"stylesheet\" type=\"text/css\" href=\""
            + fullPath + "\" />";
    }

    protected string GetJSLink(string absolutePath)
    {
        return "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" +
            absolutePath + "\"></script>";
    }
}