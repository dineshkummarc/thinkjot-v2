using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using ThinkJot.Core.Blogs;
/// <summary>
/// Summary description for IBlogEntryList
/// </summary>
public delegate void BlogEntriesDisplayedDelegate(List<BlogEntry> entries);
    
public interface IBlogEntryList
{
    event BlogEntriesDisplayedDelegate BlogEntriesDisplayed;
}
