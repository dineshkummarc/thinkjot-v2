<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"  indent="yes" omit-xml-declaration="yes" />
	<xsl:template match="/">
		<xsl:for-each select="opml/body/outline">
			<div class="blogRollEntry">
				<a href="{@xmlUrl}">
					<img src="http://www.process64.com/blogs/process64/style/rssButton.gif" border="0" alt="RSS Feed">
					</img>
				</a>
				<xsl:text disable-output-escaping="yes">   </xsl:text>
				<a href="{@htmlUrl}">
					<xsl:value-of select="@title"/>
				</a>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
