This folder is just to keep IIS happy, otherwise it will return a 404 when the ".aspx" file is not specified in the URL. 

For example http://localhost/thinkjot/ returns a 404, without this folder and the default.aspx (which can be empty, of course). Once this is in place, the request is passed on to our url-handler.