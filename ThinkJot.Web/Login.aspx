<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link media="all" rel="stylesheet" type="text/css" href="style/style.css" />
    <title>Login to ThinkJot</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="loginHeader" style="font:Arial">
            Login to ThinkJot
        </div>
        <asp:Login ID="Login1" runat="server" BackColor="#EFF3FB" BorderColor="#B5C7DE" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" Height="164px" Width="246px" BorderPadding="4" ForeColor="#333333">
            <TitleTextStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Font-Size="0.9em" />
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <TextBoxStyle Font-Size="0.8em" />
            <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px"
                Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
        </asp:Login>
        <br />
    </form>
</body>
</html>
