﻿function clearDefaultValue(box)
{
    if (box.value == box.defaultValue)  {
        box.value = "";
    }
}

function getkey(e)
{
    if (window.event)
       return window.event.keyCode;
    else if (e)
       return e.which;
    else
       return null;
}

/* Web Services */

//ERROR CODES
var wsUserNameIsNull = 2;

function wsGetValueByTag(xmlResult, tagName)    {
    return xmlResult.responseXML.getElementsByTagName(tagName)[0].firstChild.nodeValue;                        
}

// this isnt technically correct. 'Code' isnt checked in full scope.
function wsHasErrors(xmlResult) {
    var errorCode = wsGetValueByTag(xmlResult, 'Code');
    if (errorCode != 0) return true;
    return false;
}

// this isnt technically correct. 'Code' isnt checked in full scope.
function wsGetErrorCode(xmlResult) {
    return wsGetValueByTag(xmlResult, 'Code');
}

/* --- WS --- */

/* Type Conversion */
function toBool(strValue)   {
    if (strValue.toLowerCase() == "false")    {
        return false;
    }
    else return true;
}
/* --- --- */

function wsGetNodeValue(node, elementName) {
    if(node.getElementsByTagName(elementName)[0] == null)   return "";
    if(node.getElementsByTagName(elementName)[0].firstChild == null)   return "";
    
    return node.getElementsByTagName(elementName)[0].firstChild.nodeValue;
}

function wsGetSoapRequest(methodName, params)   {
    var strParams = '<' + methodName + ' xmlns="http://www.process64.com/thinkjot" >';
    if (params != null) {
        params.each(function(item)  {
            var xml = '<' + item.key + '>' + item.value + '</' + item.key + '>'
            strParams += xml;
        });
    }
    strParams += '</' + methodName + '>';
    
    //We will use SOAP 1.1    
    var request = '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
        '<soap:Body>' + 
        strParams + 
        '</soap:Body>' +
        '</soap:Envelope>';
    return request;
}

function wsGetStdOptions(wsMethod, params, onSuccessCallBack, asynchronous)  {
    var soapReq = wsGetSoapRequest(wsMethod, $H(params));
    var opt = { 
        asynchronous: asynchronous,
        method: 'post',
        postBody: soapReq,
        onSuccess: onSuccessCallBack,
        contentType: 'text/xml'                  
    }
    return opt;
}

function isLoggedIn()   {
    //We need this to be synchronous, callbacks are difficult.    
    var opt = wsGetStdOptions("IsLoggedIn", null, null, false);
    var loggedIn = false;
    var result = new Ajax.Request('/BlogService.asmx', opt); 
    strLoggedIn = wsGetValueByTag(result.transport, 'IsLoggedIn');    //(Jes)FIX: prototype bug?: not getting onSuccess cb
    if (strLoggedIn == 'false') loggedIn = false;
        else loggedIn = true;
    return loggedIn;
}  

