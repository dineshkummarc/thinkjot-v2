<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogEntriesView.ascx.cs"
    Inherits="Blogs_BlogEntriesView" %>
<%@ Register Src="BlogEntryControl.ascx" TagName="BlogEntryControl" TagPrefix="uc1" %>

<script language="javascript" type="text/javascript">
    
    ///------ Delete Blog Entry ------ ///
    function showDeleteBlogEntry(title, entryId, blogName, returnUrl)
    {
        delEntry = window.confirm("Do you want to delete the post?");
        if (delEntry)
        {
            var params =    {
                entryId: entryId,
                blogName: blogName
            }
            var opt = wsGetStdOptions("DeleteBlogEntry", params, null, false);
            //We need this to be synchronous, callbacks are difficult for deletes.        
            var result = new Ajax.Request('<%= PathUtility.CombineUrls(SysConfig.BlogsUrl, "BlogService.asmx") %>', opt);
            //we probably succeeded. :)       
            window.location.href = unescape(returnUrl);
        }
     }

    /// ----- Delete Comment ----- ///
    function showDeleteComment(targetEntryId, commentId, blogName, returnUrl)
    {
        delComment = window.confirm("Do you want to delete the comment?");
        if (delComment)
        {
            var params =    {
                entryId: targetEntryId,
                commentId: commentId,
                blogName: blogName
            }
            var opt = wsGetStdOptions("DeleteComment", params, null, false);
            //We need this to be synchronous, callbacks are difficult for deletes.        
            var result = new Ajax.Request('<%= PathUtility.CombineUrls(SysConfig.BlogsUrl, "BlogService.asmx") %>', opt);
            //we probably succeeded. :)       
            window.location.href = unescape(returnUrl);
        }
    } 
</script>

<div id="posts" class="blogEntriesView">
    <asp:PlaceHolder ID="entriesPanel" runat="server"></asp:PlaceHolder>
</div>
