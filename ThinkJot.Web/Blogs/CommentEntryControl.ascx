<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommentEntryControl.ascx.cs"
    Inherits="Blogs_CommentEntryControl" %>
<div class="addCommentBox">
    <h3 class="addCommentBoxHeader">
        Add a Comment
    </h3>
    <div class="addCommentBoxTable">
        <div id="commentErrorsBox" style="color:Red;">
            <asp:Label ID="errorLabel" runat="server" Text="Label"></asp:Label></div>
        <table class="commentEntryBoxTable">
            <tr>
                <td class="commentEntryBoxCol1">
                    Name*<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="authorTextBox"
                        ErrorMessage="RequiredFieldValidator">required</asp:RequiredFieldValidator></td>
                <td>
                    <asp:TextBox ID="authorTextBox" runat="server" Width="172px"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="commentEntryBoxCol2">
                    Email</td>
                <td>
                    <asp:TextBox ID="emailTextBox" runat="server" Width="172px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="commentEntryBoxCol1">
                    Home Page</td>
                <td class="commentEntryBoxCol2">
                    <asp:TextBox ID="homepageTextBox" runat="server" Width="192px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="commentEntryBoxCol1">
                    Comment*<br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="commentTextBox"
                        ErrorMessage="RequiredFieldValidator">required</asp:RequiredFieldValidator>
                </td>
                <td class="commentEntryBoxCol2">
                    <asp:TextBox ID="commentTextBox" runat="server" Columns="28" Rows="4" TextMode="MultiLine"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="commentEntryBoxCol1">
                    Url for details</td>
                <td class="commentEntryBoxCol2">
                    <asp:TextBox ID="detailsUrlTextBox" runat="server" Width="192px"></asp:TextBox></td>
            </tr>
            <%if (Config.UseCaptchaForComments) { %>
            <tr>
                <td class="commentEntryBoxCol1">
                    Verification*<br />
                    <asp:RequiredFieldValidator ID="captchaValidator" runat="server" ControlToValidate="captchaTextBox"
                        ErrorMessage="RequiredFieldValidator">required</asp:RequiredFieldValidator></td>
                <td class="commentEntryBoxCol2">
                    <img src="<%= PathUtility.TranslateRelativeUrl("captchaimagegen.aspx") %>" alt="Captcha" />
                    <br />
                    <asp:TextBox ID="captchaTextBox" runat="server"></asp:TextBox>
                    </td>
            </tr>
            <%} %>
        </table>
    </div>
    <asp:Button ID="saveCommentButton" runat="server" Text="Save Comment" OnClick="saveCommentButton_Click" />
</div>
