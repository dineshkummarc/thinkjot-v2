<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Blogs_Data_Process64_Default"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Src="../EntriesOnPageControl.ascx" TagName="EntriesOnPageControl" TagPrefix="uc5" %>
<%@ Register Src="~/Blogs/MonthControl.ascx" TagName="MonthControl" TagPrefix="uc4" %>
<%@ Register Src="~/Blogs/SearchControl.ascx" TagName="SearchControl" TagPrefix="uc3" %>
<%@ Register Src="~/Blogs/BlogRollControl.ascx" TagName="BlogRollControl" TagPrefix="uc2" %>
<%@ Register Src="~/Blogs/LinksControl.ascx" TagName="LinksControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "prototype.js")) %>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "blog.js")) %>
    <%= GetCssLink(PathUtility.CombineUrls(GetBlogUrl(), "style/style.css"))%>
    <%= GetCssLink(PathUtility.CombineUrls(SysConfig.StyleUrl, "blog.css"))%>
    <%= GetRssLink() %>
    <%= GetRsdLink() %>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="header">
                <h1 class="title">
                    <a href="<%= GetMappedBlogUrl() %>">Process64</a><span style="font-size: medium;
                        vertical-align: middle; padding-left: 28px;">Curried thoughts</span></h1>
            </div>
            <div id="contentPane">
                <asp:PlaceHolder ID="mainPanel" runat="server"></asp:PlaceHolder>
            </div>
            <div id="sidebar">
                <uc3:SearchControl ID="SearchBox1" runat="server" />
                <div class="getRssBlock">
                    <a href="<%= GetUrlFor("rss.xml") %>">
                        <img src="<%= GetUrlFor("style/rssButtonBig.gif") %>" alt="RSS" />
                    </a>
                </div>
                <div id="navigation">
                    <uc5:EntriesOnPageControl ID="EntriesOnPageControl1" runat="server" />
                    <uc1:LinksControl ID="LinksBox1" runat="server" />
                    <uc2:BlogRollControl ID="BlogRoll1" runat="server" />
                    <uc4:MonthControl ID="MonthViewBox" runat="server" />
                </div>
                <div class="loginStatusBox">
                    <asp:LoginStatus ID="LoginStatus1" runat="server" />
                    <asp:LoginName ID="LoginName1" runat="server" />
                </div>
            </div>
            <div id="footer">
                <p>
                    Powered by <a href="http://www.process64.com/thinkjot/">ThinkJot V2</a></p>
            </div>
        </div>
    </form>
</body>
</html>
