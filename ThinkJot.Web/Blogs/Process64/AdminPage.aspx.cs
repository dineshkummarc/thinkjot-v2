/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * 
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs;

public partial class Blogs_Data_Process64_AdminPage : BlogAdminPageBase
{
    public override Panel GetMainPanel()
    {
        return contentPanel;
    }
}