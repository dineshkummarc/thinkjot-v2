<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminPage.aspx.cs" Inherits="Blogs_Data_Process64_AdminPage"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Reference Control="~/Blogs/EditEntryControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "prototype.js")) %>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "blog.js")) %>
    <%= GetCssLink(PathUtility.CombineUrls(SysConfig.StyleUrl, "blog.css"))%>
    <%= GetCssLink(PathUtility.CombineUrls(GetBlogUrl(), "style/admin-style.css"))%>
</head>
<body>
    <div id="container">
        <form id="form1" runat="server">
            <div id="adminContentBox">
                <asp:Panel ID="contentPanel" runat="server">
                </asp:Panel>
                <p>
                    &nbsp;</p>
            </div>
        </form>
    </div>
</body>
</html>
