<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DirectTextEditControl.ascx.cs" Inherits="Blogs_DirectTextEditControl" %>
<h2>
     <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="managePostsView">
    <div class="managePostsHeader">
        <h2>
            Edit File (Be Careful)</h2>
    </div>
    <div>
        <asp:TextBox ID="rawEditTextBox" runat="server" Height="400px" TextMode="MultiLine" Width="600px"></asp:TextBox>
        <br />
        <br />
        <asp:CheckBox ID="confirmCheckBox" runat="server" Text="Confirm Changes" />
        <br />
        <br />
        <asp:Button ID="saveChangesButton" runat="server" Text="Save Changes" OnClick="saveChangesButton_Click" />&nbsp;<asp:Button
            ID="cancelButton" runat="server" OnClick="cancelButton_Click" Text="Cancel Changes" /><br />
    </div>
</div>
