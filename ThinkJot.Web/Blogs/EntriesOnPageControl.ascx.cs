/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs;

public partial class Blogs_EntriesOnPageControl : BlogControlBase
{
    public Blogs_EntriesOnPageControl()
    {
        
    }

    protected override void OnInit(EventArgs e)
    {
        ((BlogDefaultPageBase)Page).BlogEntriesDisplayed += new BlogEntriesDisplayedDelegate(OnBlogEntryListDisplayed);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void OnBlogEntryListDisplayed(List<BlogEntry> entries)
    {
        entriesRepeater.DataSource = entries;
        entriesRepeater.DataBind();
    }

    protected string GetTitleLink(object objEntry)
    {
        string blogUrl = GetMappedBlogUrl();

        BlogEntry entry = (BlogEntry)objEntry;
        //RepeatedSection:GetTitleForPost
        if (Config.UseTitleForLinks) //This means the Urls are created with title, instead of Guid
        {
            string strippedTitle = StringUtils.StripWhitespace_And_SpecialChars(entry.Title);
            return PathUtility.CombineUrls(blogUrl, strippedTitle + ".aspx");
        }
        else
        {
            return PathUtility.CombineUrls(blogUrl,
                "default.aspx?function=DisplayItem&amp;entryGuid=" + entry.UniqueId.ToString());
        }

    }

    protected string GetTitle(object objEntry)
    {
        BlogEntry entry = (BlogEntry)objEntry;
        return entry.Title;

    }
}
