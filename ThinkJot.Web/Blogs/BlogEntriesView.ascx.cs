/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs;

public partial class Blogs_BlogEntriesView : BlogControlBase, IBlogEntryList
{
    public event BlogEntriesDisplayedDelegate BlogEntriesDisplayed;

    bool parseRequest = true;
    public bool ParseRequest
    {
        get { return parseRequest; }
        set { parseRequest = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (parseRequest)
        {
            string function = Request["function"];
            switch (function)
            {
                case "DisplayDeletedItem":
                    HandleDisplayDeletedItem();
                    break;
                case "DisplayItem":
                    HandleDisplayItem();
                    break;
                case "DisplayMonth":
                    HandleDisplayMonth();
                    break;
                case "ShowCategory":
                    HandleShowCategory();
                    break;
                case "FilterByTags":
                    HandleFilterByTags();
                    break;
                default:
                    HandleDefaultView();
                    break;
            }
        }
    }

    public void LoadEntries(List<Guid> ids)
    {
        List<BlogEntry> entries = BlogEntry.GetBlogEntries(ids, BlogName);
        AddEntriesToPanel(entries);
    }

    public void LoadEntries(List<BlogEntry> entries)
    {
        AddEntriesToPanel(entries);
    }

    private void HandleDisplayDeletedItem()
    {
        string entryGuid = Request["entryGuid"];
        BlogEntry entry = BlogEntry.GetDeletedBlogEntry(new Guid(entryGuid), BlogName); 
        if (entry != null)
            LoadEntryControl(entry, true);
    }

    private void HandleDisplayItem()
    {
        BlogEntry entry = null;
        string entryGuid = Request["entryGuid"];
        if (!string.IsNullOrEmpty(entryGuid))
            entry = BlogEntry.GetBlogEntry(new Guid(entryGuid), BlogName);
        if (entry != null)
        {
            LoadEntryControl(entry, false);
            this.Page.Title = entry.Title + " - " + Config.BlogTitle;
        }
    }

    private void LoadEntryControl(BlogEntry entry, bool minimalDisplay)
    {
        if (entry == null) return;

        BlogControlBase entryControl = GetOverridableControl("BlogEntryControl.ascx");
        ((IBlogEntryControl)entryControl).LoadEntry(entry, minimalDisplay);
        entriesPanel.Controls.Add(entryControl);

    }

    private void HandleDisplayMonth()
    {
        string strMonth = Request["month"];
        string strYear = Request["year"];
        int month = Convert.ToInt32(strMonth);
        int year = Convert.ToInt32(strYear);
        List<BlogEntry> entries = BlogEntry.GetEntriesForMonth(month, year, BlogName);        
        AddEntriesToPanel(entries);
    }

    private void HandleShowCategory()
    {
        int categoryId = Convert.ToInt32(Request["categoryId"]);
        int pageNumber = 0;
        if (!string.IsNullOrEmpty(Request["page"]))
        {
            pageNumber = Convert.ToInt32(Request["page"]);
        }
        int start = pageNumber * Config.NumEntriesOnPage;
        int count = Config.NumEntriesOnPage;
        int totalItems;
        List<BlogEntry> entries = BlogEntry.GetEntriesInCategory(categoryId, start, count, out totalItems, BlogName);
        AddEntriesToPanel(entries);
        if ((totalItems > (start + count)) || (pageNumber > 0))
        {
            if (pageNumber > 0)
            {
                string strPrev = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(),
                    "default.aspx?function=ShowCategory&amp;categoryId=" + categoryId.ToString()) + "&amp;page=" + (pageNumber - 1).ToString() + "\">Back</a>";
            }
            if (totalItems > (start + count))
            {
                string strNext = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(),
                    "default.aspx?function=ShowCategory&amp;categoryId=" + categoryId.ToString()) + "&amp;page=" + (pageNumber + 1).ToString() + "\">Back</a>";
            }

            Control textControl = GetOverridableControl("SimpleTextDisplayControl.ascx");
            ((ISimpleTextDisplayControl)textControl).LoadText("<div class=\"paginationBox\">" + "Page " + (pageNumber + 1).ToString() + " of "
                + (totalItems / count).ToString() + "</div>");
            entriesPanel.Controls.Add(textControl);
        }
    }

    private void HandleFilterByTags()
    {
        string tag = Request["tag"];
        int pageNumber = 0;
        if (!string.IsNullOrEmpty(Request["page"]))
        {
            pageNumber = Convert.ToInt32(Request["page"]);
        }
        int start = pageNumber * Config.NumEntriesOnPage;
        int count = Config.NumEntriesOnPage;
        int totalItems;
        List<BlogEntry> entries = BlogEntry.GetEntriesByTag(tag, start, count, out totalItems, BlogName);
        AddEntriesToPanel(entries);
        if ((totalItems > (start + count)) || (pageNumber > 0))
        {
            string strPrev = "";
            if (pageNumber > 0)
            {
                strPrev = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(),
                    "default.aspx?function=FilterByTags&amp;tag=" + Server.UrlEncode(tag)) + "&amp;page=" + (pageNumber - 1).ToString() + "\">Back</a>";
            }
            string strNext = "";
            if (totalItems > (start + count))
            {
                strNext = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(),
                    "default.aspx?function=FilterByTags&amp;tag=" + Server.UrlEncode(tag)) + "&amp;page=" + (pageNumber + 1).ToString() + "\">Next</a>";
            }

            Control textControl = GetOverridableControl("SimpleTextDisplayControl.ascx");
            ((ISimpleTextDisplayControl)textControl).LoadText("<div class=\"paginationBox\">" + strPrev + " Page " + (pageNumber + 1).ToString() + " of "
                + (totalItems / count).ToString() + " " + strNext + "</div>");
            entriesPanel.Controls.Add(textControl);
        }
    }

    private void HandleDefaultView()
    {
        List<BlogEntry> entries = BlogEntry.GetLastNormalTypeEntries(Config.NumEntriesOnPage, false, BlogName);
        AddEntriesToPanel(entries);
    }

    private void AddEntriesToPanel(List<BlogEntry> entries)
    {
        BlogControlBase entryControl;
        foreach (BlogEntry entry in entries)
        {
            if (!entry.Publish || entry.ExcludeFromList) continue;
            entryControl = GetOverridableControl("BlogEntryControl.ascx");
            ((IBlogEntryControl)entryControl).LoadEntry(entry, false);
            entriesPanel.Controls.Add(entryControl);
        }

        //Notification Event
        if (BlogEntriesDisplayed != null)
            BlogEntriesDisplayed(entries);
    }
}