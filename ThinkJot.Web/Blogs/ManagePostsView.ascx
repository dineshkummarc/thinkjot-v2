<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagePostsView.ascx.cs"
    Inherits="Blogs_ManagePostsView" %>
<h2>
     <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="managePostsView">
    <div class="managePostsHeader">
        <h2>
            Manage Posts
        </h2>
    </div>
    <asp:GridView ID="entriesGrid" GridLines="None" ForeColor="#333333" CellPadding="4"
        AutoGenerateColumns="False" runat="server" OnRowDataBound="entriesGrid_RowDataBound"
        OnRowDeleting="entriesGrid_RowDeleting" Width="700px" OnRowEditing="entriesGrid_RowEditing">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Date" SortExpression="Time">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Time") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemStyle Width="340px" />
                <HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="titleLabel" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="Author">
                <ItemTemplate>
                    <asp:Label ID="authorLabel" runat="server" Text='<%# Bind("Author") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Publish" SortExpression="Publish">
                <ItemTemplate>
                    <asp:CheckBox ID="publishCheckBox" runat="server" Checked='<%# Bind("Publish") %>'
                        Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" SortExpression="UniqueId" ShowHeader="false">
                <ItemTemplate>
                    <asp:HiddenField ID="entryIdHiddenField" runat="server" Value='<%# Bind("UniqueId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Edit" />&nbsp;<asp:Button ID="Button2" runat="server" CausesValidation="False"
                            CommandName="Delete" Text="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
