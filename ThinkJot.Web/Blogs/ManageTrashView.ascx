<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageTrashView.ascx.cs"
    Inherits="Blogs_ManageTrashView" %>
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="manageTrashView">
    <div class="manageTrashHeader">
        <h2>
            Trash Folder
        </h2>
    </div>
    <asp:GridView ID="entriesGrid" GridLines="None" ForeColor="#333333" CellPadding="4"
        AutoGenerateColumns="False" runat="server" OnRowDeleting="entriesGrid_RowDeleting"
        Width="700px" OnRowDataBound="entriesGrid_RowDataBound" EnableViewState="False" AllowSorting="True" EnableTheming="False">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Date" SortExpression="Time">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Time") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Title" SortExpression="Title">
                <ItemStyle Width="340px" />
                <HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="titleLabel" runat="server" Text='<%# GetEntryLink(Container.DataItem) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Author" SortExpression="Author">
                <ItemTemplate>
                    <asp:Label ID="authorLabel" runat="server" Text='<%# Bind("Author") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="UniqueId" ShowHeader="False">
                <ItemTemplate>
                    <asp:HiddenField ID="entryIdHiddenField" runat="server" Value='<%# Bind("UniqueId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Recover Post" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    &nbsp;<asp:Button ID="emptyTrashButton" runat="server" OnClick="emptyTrashButton_Click"
        Text="Empty Trash" /></div>
