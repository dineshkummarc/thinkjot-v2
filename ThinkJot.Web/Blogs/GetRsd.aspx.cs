/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;

using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs;
using ThinkJot.Core.Utils;

public partial class Blogs_GetRsd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string blogName = Request["blogName"];
        Response.Clear();
        Response.ContentType = "text/xml";
        Response.Write(GetRsdDoc(blogName));
        Response.End();
    }

    private string GetRsdDoc(string blogName)
    {
        PathUtility pathUtil = new PathUtility();
        string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        xml += "<rsd version=\"1.0\" xmlns=\"http://archipelago.phrasewise.com/rsd\">";
        xml += "<service xmlns=\"\"><engineName>ThinkJot v2.0</engineName>";
        xml += "<engineLink>http://www.process64.com/thinkjot/</engineLink>";
        xml += "<homePageLink>" + pathUtil.GetMappedBlogUrl(blogName) + "</homePageLink>";
        xml += "<apis><api name=\"MetaWeblog\" preferred=\"true\" " +
            "apiLink=\"" + pathUtil.CombineUrls(SystemConfiguration.GetConfig().BlogsUrl, "metaweblogapi.aspx") +
            "\" blogID=\"" + blogName + "\" />";
        xml += "</apis></service></rsd>";
        return xml;
    }
}
