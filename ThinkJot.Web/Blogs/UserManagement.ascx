<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserManagement.ascx.cs"
    Inherits="Blogs_UserManagement" %>
<!-- Navigation -->
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<!-- Header -->
<div class="UserManagement">
    <div id="UserManagement_Header">
        User Management</div>
    This control is incomplete. Edit is not implemented yet.
    <asp:Repeater ID="usersRepeater" runat="server">
        <HeaderTemplate>
            <table>
                <tr>
                    <td>
                        UserName</td>
                    <td>
                        Password</td>
                    <td>
                        Email</td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                </td>
                <td>
                    <%# DataBinder.Eval(Container.DataItem, "Password")%>
                </td>
                <td>
                    <%# DataBinder.Eval(Container.DataItem, "Email")%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
