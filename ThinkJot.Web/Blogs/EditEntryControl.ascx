<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditEntryControl.ascx.cs"
    Inherits="Blogs_EditEntryControl" %>

<script language="javascript" type="text/javascript" src="<%= GetTinyMCEPath() %>"></script>

<script language="javascript" type="text/javascript" src="<%= GetCalendarPopupPath() %>"></script>

<script language="javascript" type="text/javascript">
    tinyMCE.init({
	    mode : "exact",
	    elements : "contentBox",
        theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu,paste,directionality,fullscreen",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "example_word.css",
	    plugi2n_insertdate_dateFormat : "%Y-%m-%d",
	    plugi2n_insertdate_timeFormat : "%H:%M:%S",
		external_link_list_url : "example_link_list.js",
		external_image_list_url : "example_image_list.js",
		flash_external_list_url : "example_flash_list.js",
		file_browser_callback : "fileBrowserCallBack",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : false,
		paste_remove_styles : false,
		convert_urls : false
    });

    var cal = new CalendarPopup();
    
    function showAddCategory(blogName)
    {
        var name = prompt("Enter the Category Name", "Name");
        if (name == null || name == '') return;
        var params =    {
            categoryName: name,
            categoryDescription: name, //for now, let them be the same!
            blogName: blogName
        }
        var opt = wsGetStdOptions("AddCategory", params, null, false);
        //We need this to be synchronous, callbacks are difficult for deletes.        
        var result = new Ajax.Request('<%= PathUtility.CombineUrls(SysConfig.BlogsUrl, "BlogService.asmx") %>', opt);
        //we probably succeeded. :)
        window.location.reload();
    }    
</script>
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="editEntryBox">
    <h2>
        <%= GetEntryMode() %>
    </h2>
    <div class="editEntryContainer">
        <table width="720" cellpadding="8">
            <tr class="editEntryTableRow">
                <td style="width: 120px; height: 26px;">
                    Title
                </td>
                <td style="width: 265px; height: 26px;">
                    <asp:TextBox ID="titleTextBox" runat="server" Width="423px"></asp:TextBox></td>
            </tr>
            <% if (!Config.DisableCategories) { %>
            <tr class="editEntryTableRowAlternating">
                <td style="width: 120px; vertical-align: top">
                    Category
                </td>
                <td style="width: 265px">
                    <asp:CheckBoxList ID="categoryBoxes" runat="server" DataTextField="Name" DataValueField="Id"
                        RepeatColumns="2">
                    </asp:CheckBoxList>
                    <span class="linklessLink" onclick="showAddCategory('<%= BlogName %>')">Add Category</span>
                </td>
            </tr>
            <% } %>
            <tr class="editEntryTableRowAlternating">
                <td style="vertical-align: top; width: 120px">
                    Tags<br />
                    (eg: linux, india)</td>
                <td style="width: 465px">
                    <asp:TextBox ID="tagsTextBox" runat="server" Width="200px"></asp:TextBox>
                    (comma seperated)</td>
            </tr>
            <tr class="editEntryTableRow">
                <td style="width: 120px; height: 9px; vertical-align: top">
                    Description
                </td>
                <td style="width: 265px; height: 9px">
                    <asp:TextBox ID="descriptionTextBox" runat="server" Rows="3" Width="548px" TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <% if (!IsNewEntry()) {%>
            <tr class="editEntryTableRowAlternating">
                <td style="vertical-align: top; width: 120px; height: 9px">
                    Date</td>
                <td style="height: 9px">
                    <input id="dateBox" name="dateBox" type="text" value="<%= GetDateText() %>" />
                    <a href="#" onclick="cal.select(document.forms[0].dateBox,'dateAnchor','yyyy/MM/dd'); return false;"
                        id="dateAnchor">Change (yyyy/MM/dd)</a></td>
            </tr>
            <% } %>
            <% if (!IsNewEntry())
               {%> 
            <tr class="editEntryTableRow">
                <td style="vertical-align: top; width: 120px; height: 9px">
                    Time</td>
                <td style="width: 265px; height: 9px">
                <%} %>
                    <asp:TextBox ID="timeTextBox" runat="server"></asp:TextBox>
            <% if (!IsNewEntry())
               {%>                     
                    </td>
            </tr>
            <% } %>
            <tr class="editEntryTableRowAlternating">
                <td style="width: 120px; height: 35px;">
                    Content
                </td>
                <td style="width: 265px; height: 35px;">
                </td>
            </tr>
            <tr class="editEntryTableRow">
                <td colspan="2" style="height: 330px">
                    &nbsp;<textarea id="contentBox" name="contentBox" cols="80" rows="20"><%= GetContentText() %></textarea>
                </td>
            </tr>
            <tr class="editEntryTableRow">
                <td style="width: 120px">
                    Options</td>
                <td style="width: 500px">
                    &nbsp;<asp:CheckBox ID="syndicateCheckBox" runat="server" Checked="True" Text="Syndicate" />
                    |
                    <asp:CheckBox ID="allowCommentsCheckBox" runat="server" Checked="True" Text="Show Comments" />
                    |
                    <asp:CheckBox ID="closeComments" runat="server" Text="Lock Comments" />
                    |
                    <asp:CheckBox ID="excludeFromListCheckBox" runat="server" Checked="False" Text="Hide" />
                    <br />
                    Disable Comments After:
                    <asp:DropDownList ID="disableCommentDaysCheckBox" runat="server" Width="122px">
                        <asp:ListItem Selected="True" Value="-1">Don't Disable</asp:ListItem>
                        <asp:ListItem Value="1">1 day</asp:ListItem>
                        <asp:ListItem Value="3">3 days</asp:ListItem>
                        <asp:ListItem Value="7">1 Week</asp:ListItem>
                        <asp:ListItem Value="14">2 Weeks</asp:ListItem>
                        <asp:ListItem Value="28">4 Weeks</asp:ListItem>
                        <asp:ListItem Value="90">3 Months</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr class="editEntryTableRowAlternating">
                <td style="width: 120px; height: 34px;">
                </td>
                <td style="width: 400px; height: 34px;">
                    <asp:Button ID="saveEntryButton" runat="server" OnClick="saveEntryButton_Click" Text="Publish" />
                    <asp:Button ID="saveDraftButton" runat="server" OnClick="saveDraftButton_Click" Text="Save Draft" />
                    <asp:Button ID="saveArticleButton" runat="server" OnClick="saveArticleButton_Click" Text="Save as Article" />
                    <asp:Button ID="cancelButton" runat="server" OnClick="cancelButton_Click" Text="Cancel"
                        Width="87px" />
                </td>
            </tr>
        </table>
        <br />
        <table width="720" cellpadding="8">
            <tr class="editEntryTableRow">
                <td style="width: 106px; height: 26px;">
                    <strong>Attachments</strong></td>
                <td style="width: 500px; height: 26px;">
                </td>
            </tr>
            <tr class="editEntryTableRowAlternating">
                <td style="width: 106px; height: 93px;">
                    Upload Image</td>
                <td style="width: 500px; height: 93px;">
                    <table style="width: 300px">
                        <tr>
                            <td colspan="1" style="width: 388px">
                                <asp:FileUpload ID="imageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td style="width: 388px">
                                Alternate Text:
                                <asp:TextBox ID="altTextTextBox" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 388px">
                                <asp:Button ID="imageUploadButton" runat="server" Text="Upload Image" Width="105px"
                                    OnClick="imageUploadButton_Click" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="editEntryTableRow">
                <td style="width: 106px; height: 92px;">
                    Upload File</td>
                <td style="width: 500px; height: 92px;">
                    <table style="width: 300px">
                        <tr>
                            <td style="width: 302px">
                                File Type:&nbsp;<asp:DropDownList ID="uploadTypeList" runat="server" Width="102px">
                                    <asp:ListItem>Video</asp:ListItem>
                                    <asp:ListItem>Other</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="height: 26px; width: 302px;">
                                <asp:FileUpload ID="fileUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td style="width: 302px">
                                <asp:Button ID="fileUploadButton" runat="server" Text="Upload" Width="86px" OnClick="fileUploadButton_Click" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
