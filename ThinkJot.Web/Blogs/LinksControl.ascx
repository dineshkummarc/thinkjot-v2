<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinksControl.ascx.cs"
    Inherits="Blogs_LinksControl" %>
<%@ OutputCache Duration="120" VaryByParam="None" %>
<asp:Repeater ID="linksRepeater" runat="server">
    <HeaderTemplate>
        <div class="webLinksBox">
            <h3>
                Links</h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li class="linkBoxEntry"><a href="<%# DataBinder.Eval(Container.DataItem, "Url")%>">
            <%# DataBinder.Eval(Container.DataItem,"Name") %>
        </a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
