<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageFilesView.ascx.cs"
    Inherits="Blogs_ManageFilesView" %>
<h2>
     <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="manageFilesView">
    <div class="manageFilesHeader">
        <h2>
            Manage Files
        </h2>
    </div>
    <asp:GridView ID="filesGrid" GridLines="None" ForeColor="#333333" CellPadding="4"
        AutoGenerateColumns="False" runat="server" OnRowDeleting="filesGrid_RowDeleting"
        Width="700px" OnRowDataBound="filesGrid_RowDataBound">
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#EFF3FB" />
        <EditRowStyle BackColor="#2461BF" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="File Name" SortExpression="FileName">
                <ItemStyle Width="340px" />
                <HeaderStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="fileNameLabel" runat="server" Text=''></asp:Label>                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="" SortExpression="FileName" ShowHeader="false">
                <ItemTemplate>
                    <asp:HiddenField ID="fileNameHiddenField" runat="server" Value='<%# Bind("FileName") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="File Date" SortExpression="FileDate">
                <ItemTemplate>
                    <asp:Label ID="fileDate" runat="server" Text='<%# Bind("FileDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="deleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
