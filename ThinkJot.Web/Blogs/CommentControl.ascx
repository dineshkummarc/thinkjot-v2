<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommentControl.ascx.cs"
    Inherits="Blogs_CommentControl" %>
<li class="comment">
    <%= GetCommentEditText()%>
    <p class="commentAuthor">
        On
        <asp:Label ID="dateLabel" runat="server" Text="date" CssClass="commentDateLabel"></asp:Label>,
        <asp:Label ID="nameLabel" runat="server" Text="Name" CssClass="commentNameLabel"></asp:Label>
        said:
    </p>
    <p class="commentText">
        <asp:Label ID="commentLabel" runat="server" Text="Comment" CssClass="commentTextLabel"></asp:Label>
    </p>
    <p class="commentDetailsUrl">
        <asp:Label ID="externalLinkLabel" runat="server" Text="Comment" CssClass="commentExternalLinkLabel"></asp:Label>
    </p>
</li>
