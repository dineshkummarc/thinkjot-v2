<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControlPanel.ascx.cs"
    Inherits="Blogs_ControlPanel" %>
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a></h2>
<div class="controlPanel">
    <div class="controlPanelHeader">
        <h2>
            Control Panel
        </h2>
    </div>
    <div class="controlPanelEntry">
        <img src="<%= GetIcon("ManageContent") %>" alt="ManageContent" style="float: left" />
        <div class="controlPanelEntryText">
            <h2>
                Manage Content
            </h2>
            Manage the content of your blog. Create new content, or edit existing posts, attachments
            and comments.&nbsp;<br />
            <ul>
                <li><a href="<%= GetLink("NewPost") %>">New Post</a></li>
                <li><a href="<%= GetLink("ManagePosts") %>">Manage Posts</a> - Edit existing posts,
                    Publish, Syndicate etc. </li>
                <li><a href="<%= GetLink("ManageFiles") %>">Manage Files and Attachments</a> - Edit/Remove
                    attachments and media.</li>
                <li><a href="<%= GetLink("ManageTrash") %>">Trash Folder</a> - Restore deleted content
                    from Trash, Empty Trash.</li>
            </ul>
        </div>
    </div>
    <div class="controlPanelEntry">
        <img src="<%= GetIcon("Activity") %>" alt="Activity" style="float: left" />
        <div class="controlPanelEntryText">
            <h2>
                Activity
            </h2>
            Look at various usage statistics for your blog. You need set EnableActivityTracking
            = true in blog.config for this to work.&nbsp;<br />
            <ul>
                <li><a href="<%= GetLink("ViewReferrers") %>">Referrers</a> - Which sites and search
                    engines are referring you?</li>
                <li><a href="<%= GetLink("ViewSearchEngineActivity") %>">Search Engines</a> - See Search
                    Engine related information for the blog.</li>
                <li><a href="<%= GetLink("ViewTopPagesAndUserAgents") %>">Top Pages and User Agents</a>
                    - See the most requested pages and the User Agents being used on the site.</li>
                <li><a href="<%= GetLink("ViewVisitors") %>">View Visitors</a> - See the IP address
                    of site visitors, and the number of pages requested.</li>
                <li><a href="<%= GetLink("ViewEvents") %>">Events</a> - Event log contains important
                    system level activities on your blog. You should check it often.</li>
            </ul>
        </div>
    </div>
    <div class="controlPanelEntry">
        <img src="<%= GetIcon("SystemSettings") %>" alt="System Settings" style="float: left" />
        <div class="controlPanelEntryText">
            <h2>
                Blog Settings
            </h2>
            Edit static content in you blog, RSS and Atom feeds etc.
            <br />
            <ul>
                <li><a href="<%= GetLink("MigrateDasBlog") %>">Migrate from dasBlog 1.8</a> (buggy)</li>
                <li><a href="<%= GetLink("EditBlogRoll") %>">Edit Blog Roll</a></li>
                <li><a href="<%= GetLink("EditWebLinks") %>">Edit Links</a></li>
                <li><a href="<%= GetLink("EditCategories") %>">Edit Categories</a></li>
                <li><a href="<%= GetLink("BlogConfigCommon") %>">Edit Common Blog Config Options</a></li>
                <li><a href="<%= GetLink("EditBlogConfig") %>">Edit Blog Config</a> (Raw XML Edit, risky)</li>
                <li><a href="<%= GetLink("ReloadBlogConfig") %>">Reload Blog Config</a></li>
                <li><a href="<%= GetLink("ManageFeeds") %>">Manage Feeds</a> - To be implemented. Manage
                    your RSS and Atom feeds.</li>
            </ul>
        </div>
    </div>
    <div class="controlPanelEntry">
        <img src="<%= GetIcon("Security") %>" alt="Security" style="float: left" />
        <div class="controlPanelEntryText">
            <h2>
                Security (only for System Administrators, login=administrator)
            </h2>
            Manage users and edit permissions for this blog.
            <br />
            <ul>
                <li><a href="<%= GetLink("UserManagement") %>">User Management</a> - Manage users, 
                their passwords and other information.</li>
                <li><a href="<%= GetLink("ManageEditors") %>">Manage Editors</a> - To be implemented.
                    For now, edit blog.config in ~/App_Data/Blogs/&lt;blog_name&gt;</li>
            </ul>
        </div>
    </div>
</div>
