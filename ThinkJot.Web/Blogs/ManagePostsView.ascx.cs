/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs;

public partial class Blogs_ManagePostsView : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) BindData();
    }

    protected void entriesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string blogUrl = GetMappedBlogUrl();
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            BlogEntrySummary summary = (BlogEntrySummary)e.Row.DataItem;

            e.Row.Cells[0].Text = summary.Time.ToShortDateString();
            if (Config.UseTitleForLinks)
            {
                string strippedTitle = StringUtils.StripWhitespace_And_SpecialChars(summary.Title);
                e.Row.Cells[1].Text = "<a href=\"" + PathUtility.CombineUrls(blogUrl, strippedTitle + ".aspx") +
                "\">" + Server.HtmlEncode(summary.Title) + "</a>";
            }
            else
            {
                e.Row.Cells[1].Text = "<a href=\"" + PathUtility.CombineUrls(
                    blogUrl, "default.aspx?function=DisplayItem&amp;entryGuid=" + summary.UniqueId.ToString()) +
                    "\">" + Server.HtmlEncode(summary.Title) + "</a>";
            }
        }
    }

    protected void entriesGrid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridViewRow row = entriesGrid.Rows[e.NewEditIndex];
        HiddenField entryField = (HiddenField)row.FindControl("entryIdHiddenField");
        Response.Redirect("adminpage.aspx?function=EditEntry&entryId=" + entryField.Value);
    }

    protected void entriesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = entriesGrid.Rows[e.RowIndex];
        HiddenField entryField = (HiddenField)row.FindControl("entryIdHiddenField");
        BlogEntry.DeleteEntry(new Guid(entryField.Value), BlogName);
        BindData();
    }

    private void BindData()
    {
        entriesGrid.DataSource = BlogEntry.GetLastEntriesSummary(500, BlogName);//500 = just a big number!
        entriesGrid.DataBind();
    }
}