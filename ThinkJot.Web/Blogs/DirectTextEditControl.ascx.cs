using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Blogs;

public partial class Blogs_DirectTextEditControl : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            LoadTextForEdit();
    }

    private void LoadTextForEdit()
    {
        string settingName = Request["settingName"];
        string contents = RawTextEditor.GetRawSettingsText(settingName, BlogName);
        ViewState["settingName"] = settingName;
        rawEditTextBox.Text = contents;
    }

    protected void saveChangesButton_Click(object sender, EventArgs e)
    {
        string settingName = (string)ViewState["settingName"];
        
        if (string.IsNullOrEmpty(settingName))
            return;

        if (!confirmCheckBox.Checked)
        {
            confirmCheckBox.Text = "Please confirm the changes since a Direct Edit is potentially dangerous.";
            return;
        }
        RawTextEditor.PersistRawSettingsText(settingName, rawEditTextBox.Text, BlogName);

        Response.Redirect("adminpage.aspx?function=ControlPanel");
    }
    protected void cancelButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("adminpage.aspx?function=ControlPanel");
    }
}
