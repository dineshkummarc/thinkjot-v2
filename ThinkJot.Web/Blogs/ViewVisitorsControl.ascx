<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewVisitorsControl.ascx.cs" Inherits="Blogs_ViewVisitorsControl" %>
<script language="javascript" type="text/javascript" src="<%= GetCalendarPopupPath() %>"></script>
    <script language="javascript" type="text/javascript">
        var cal = new CalendarPopup();
    </script>
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="viewActivityLogControl">
    <div class="viewActivityLogHeader">
        <h2>
            Visitors
        </h2>
    </div>
    <div>
        <input id="dateBox" name="dateBox" type="text" value="<%= GetDateText() %>" />
        <a href="#" onclick="cal.select(document.forms[0].dateBox,'dateAnchor','yyyy/MM/dd'); return false;"
            id="dateAnchor">Change</a> (yyyy/MM/dd)</div>
    <br />
    <asp:Button ID="refreshButton" runat="server" Text="Display Results" OnClick="refreshButton_Click" />
    <br />
    <div id="fullVisitorsList">
        <h3>
            Visitors List</h3>
        <%= GetVisitorsHtml() %>
    </div>
</div>
