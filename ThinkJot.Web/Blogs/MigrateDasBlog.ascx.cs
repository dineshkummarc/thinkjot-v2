/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs.Migration.DasBlog_1_8;

public partial class Blogs_MigrateDasBlog : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void migrateButton_Click(object sender, EventArgs e)
    {
        MigrationUtil migrator = new MigrationUtil();
        migrator.Run(Request.MapPath("~/App_Data/Blogs/XmlData/" + BlogName + "/dasBlog_Content"), BlogName);
    }
}
