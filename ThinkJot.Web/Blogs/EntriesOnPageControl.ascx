<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EntriesOnPageControl.ascx.cs"
    Inherits="Blogs_EntriesOnPageControl" %>
<asp:Repeater ID="entriesRepeater" runat="server">
    <HeaderTemplate>
        <div class="entriesOnPageBox">
            <h3 class="entriesOnPageHeader">
                On this page...</h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li class="entriesOnPageEntry"><a href="<%# GetTitleLink(Container.DataItem) %>">
            <%# GetTitle(Container.DataItem)%>
        </a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
