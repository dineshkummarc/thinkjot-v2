<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Blogs_Data_Process64_Default"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Src="../EntriesOnPageControl.ascx" TagName="EntriesOnPageControl" TagPrefix="uc5" %>
<%@ Register Src="~/Blogs/MonthControl.ascx" TagName="MonthControl" TagPrefix="uc4" %>
<%@ Register Src="~/Blogs/SearchControl.ascx" TagName="SearchControl" TagPrefix="uc3" %>
<%@ Register Src="~/Blogs/BlogRollControl.ascx" TagName="BlogRollControl" TagPrefix="uc2" %>
<%@ Register Src="~/Blogs/LinksControl.ascx" TagName="LinksControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "prototype.js")) %>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "blog.js")) %>
    <%= GetCssLink(PathUtility.CombineUrls(GetBlogUrl(), "style/style.css"))%>
    <%= GetCssLink(PathUtility.CombineUrls(SysConfig.StyleUrl, "blog.css"))%>
    <%= GetRssLink() %>
    <%= GetRsdLink() %>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="header">
                <h1 class="title">
                    <a href="<%= GetMappedBlogUrl() %>">ThinkJot</a><span style="font-size:medium;vertical-align:middle;padding-left:28px;">Blog Software for Asp.Net and Mono</span></h1>
            </div>
            <div id="contentPane">
                <asp:PlaceHolder ID="mainPanel" runat="server"></asp:PlaceHolder>
            </div>
            <div id="sidebar">
                <uc3:SearchControl ID="SearchBox1" runat="server" />
                <div class="getRssBlock">
                    <a href="<%= GetUrlFor("rss.xml") %>">
                        <img src="<%= GetUrlFor("style/rssButtonBig.gif") %>" alt="RSS" />
                    </a>
                </div>
                <div id="navigation">
                <div id="tjLinks">
                    <ul>
                        <li><a href="default.aspx">Home</a></li>
                        <li><a href="http://code.google.com/p/thinkjot/">Source Code</a></li>
                        <li><a href="ThinkJotV2Features.aspx">Features</a></li>
                        <li><a href="screenshots.aspx">Screen Shots</a></li>
                        <li><a href="downloadthinkjotv2.aspx">Download</a></li>
                        <li><a href="Roadmap.aspx">Roadmap</a></li>
                        <li><a href="contribute.aspx">Contribute</a></li>
                        <li><a href="http://www.process64.com">Process64</a></li>
                    </ul>
                </div>
                    <uc4:MonthControl ID="MonthViewBox" runat="server" />
                </div>
                <div class="loginStatusBox">
                    <asp:LoginStatus ID="LoginStatus1" runat="server" />
                    <asp:LoginName ID="LoginName1" runat="server" />
                </div>
            </div>
            <div id="footer">
                <p>
                    Powered by <a href="http://www.process64.com/thinkjot/">ThinkJot V2</a></p>
            </div>
        </div>
    </form>
</body>
</html>
