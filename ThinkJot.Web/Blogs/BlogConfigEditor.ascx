<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogConfigEditor.ascx.cs"
    Inherits="Blogs_BlogConfigEditor" %>
<h2>
    <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="blogConfigEditor">
    <div class="manageFilesHeader">
        <h2>
            Edit blog.Config (Be Careful)
        </h2>
        This Control is not ready yet. You should edit blog.config directly. 
        <div>
            &nbsp;</div>
    </div>
</div>
<%--    </div>
    <div id="blogConfigEditForm">
        <table style="width: 600px;">
            <tr>
                <td class="configEditTableCell" style="width: 209px">
                    Title</td>
                <td class="configEditTableCell" style="width: 4px">
                    <asp:TextBox ID="titleTextBox" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px; height: 26px;">
                    Alternate Url</td>
                <td class="configEditTableCell" style="width: 4px; height: 26px;">
                    <asp:TextBox ID="alternateUrlTextBox" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px; height: 26px;">
                    Administrator (username)</td>
                <td class="configEditTableCell" style="width: 4px; height: 26px;">
                    <asp:TextBox ID="administratorTextBox" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px">
                    Editors (comma seperated usernames)</td>
                <td class="configEditTableCell" style="width: 4px">
                    <asp:TextBox ID="editorsTextBox" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px">
                    Use Title For Links</td>
                <td class="configEditTableCell" style="width: 4px">
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px">
                    Num Entries on Page</td>
                <td class="configEditTableCell" style="width: 4px">
                    <asp:TextBox ID="TextBox1" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell" style="width: 209px">
                    Num Items in Feed</td>
                <td class="configEditTableCell" style="width: 4px">
                    <asp:TextBox ID="TextBox2" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Max Search Results</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox3" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Show Comment Count</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Use Captcha - For Comments</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList3" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Auto Save Posts</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList4" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Contact</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox4" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Contact Email</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox5" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Copyright</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox6" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Enable MetaWeblog API</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList5" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Ping Update Trackers</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList6" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Enable Activity Tracking</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList7" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Email author on comments</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList8" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Smtp Server Name</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox7" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Smtp Server Port</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox8" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Smtp Auth Required</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList9" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Smtp UserName</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox10" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Smtp Password</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox9" runat="server" Width="201px"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Disable Categories</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList10" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Use Universal Time (UTC)</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList11" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    Use Server Local Time</td>
                <td class="configEditTableCell">
                    <asp:DropDownList ID="DropDownList12" runat="server">
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="configEditTableCell">
                    UTC Offsite (+hh:mm) or (-hh:mm)</td>
                <td class="configEditTableCell">
                    <asp:TextBox ID="TextBox11" runat="server" MaxLength="60" Width="300px"></asp:TextBox></td>
            </tr>
        </table>
    </div>
</div>--%>
