<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdminBar.ascx.cs" Inherits="Blogs_AdminBar" %>
<div class="adminBar">
    <% if (IsBlogAdministrator()) { %>
        <a href="<%= GetControlPanelLink()%>">Control Panel</a> | 
    <%} %>
    <a href="<%= GetAddEntryLink()%>">New Post</a>
</div>
