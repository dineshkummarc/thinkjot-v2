/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;

public partial class Blogs_ManageTrashView : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    protected void entriesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = ((BlogEntry)e.Row.DataItem).Time.ToShortDateString();
        }
    }

    protected void entriesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = entriesGrid.Rows[e.RowIndex];
        HiddenField entryField = (HiddenField)row.FindControl("entryIdHiddenField");
        BlogEntry.Undelete(new Guid(entryField.Value), BlogName);
        LoadData();
    }

    private void LoadData()
    {
        entriesGrid.DataSource = BlogEntry.GetDeletedEntries(BlogName);
        entriesGrid.DataBind();
    }

    protected string GetEntryLink(object item)
    {
        BlogEntry summary = (BlogEntry)item;
        return "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(), "default.aspx?function=DisplayDeletedItem&amp;entryGuid=" + summary.UniqueId.ToString()) +
            "\">" + summary.Title + "</a>";
    }

    protected void emptyTrashButton_Click(object sender, EventArgs e)
    {
        Trash.Empty(BlogName);
        LoadData();
    }
}