/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;
using ThinkJot.Core.Utils;

public partial class Blogs_CommentControl : BlogControlBase, ICommentControl
{
    Comment comment;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void LoadComment(Comment comment)
    {
        this.comment = comment;

        //date stuff, utc blah blah
        DateTime displayTime;
        if (Config.UseUniversalTime)
            displayTime = comment.Time;
        else
        {
            if (Config.UseServerLocalTime)
                displayTime = comment.Time.ToLocalTime();
            else
                displayTime = DateTimeUtility.AddTimeOffset(comment.Time, Config.UtcOffset);
        }
        dateLabel.Text = displayTime.ToShortDateString();        
        //date stuff ends here...

        commentLabel.Text = GetFormattedCommentText(comment.Text);

        if (string.IsNullOrEmpty(comment.AuthorHomepage)) nameLabel.Text = Server.HtmlEncode(comment.Author);
        else
        {
            string authorHomepage = comment.AuthorHomepage.ToLower().StartsWith("http://") ? comment.AuthorHomepage : "http://" + comment.AuthorHomepage;
            nameLabel.Text = "<a href=\"" + Server.HtmlEncode(authorHomepage) + "\" rel=\"nofollow\">" + Server.HtmlEncode(comment.Author) + "</a>";
        }
        
        if (string.IsNullOrEmpty(comment.ExternalLink)) externalLinkLabel.Visible = false;
        else
        {
            string link = comment.ExternalLink;
            link = link.ToLower().StartsWith("http://") ? link : "http://" + link;
            externalLinkLabel.Text = "Details at: <a href=\"" + Server.HtmlEncode(link) + "\" class=\"commentExternalLink\" rel=\"nofollow\">" + Server.HtmlEncode(link) + "</a>";
        }
    }

    protected string GetCommentEditText()
    {
        if (!new SecurityUtility().IsBlogEditor(BlogName)) return "";
        string text = "<p class=\"blogCommentEditLinks\">Admin: " +
            "<span class=\"linklessLink\" onclick=\"showDeleteComment('" + comment.TargetEntryId + "', '" + comment.UniqueId + "', '" + BlogName + "', '" + Server.UrlEncode(Request.Url.AbsoluteUri) + "');\">Delete Comment</span></p>";
        return text;
    }

    private string GetFormattedCommentText(string text)
    {
        if (string.IsNullOrEmpty(text))
            return "";

        //SECTION: AddBR
        //This code replaces newlines with <br />
        string[] parts = text.Split(new string[]{"\r\n", "\n"}, StringSplitOptions.None);
        List<string> partList = new List<string>();

        bool foundBlank = false;
        foreach(string part in parts)
        {
            if (string.IsNullOrEmpty(part))
            {
                //dont allow two blanks together!
                if (!foundBlank)
                {
                    partList.Add(part);
                }
                foundBlank = true;
            }
            else
            {
                partList.Add(Server.HtmlEncode(part));
                foundBlank = false;
            }
        }

        if (partList.Count > 0)
            return string.Join("<br />", partList.ToArray());
        else
            return "";
        //END SECTION: AddBR
    }
}