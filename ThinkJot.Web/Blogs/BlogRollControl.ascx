<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogRollControl.ascx.cs" Inherits="Blogs_BlogRollControl" %>
<%@ OutputCache Duration="120" VaryByParam="None" %>
<div class="blogRollBox">
    <h3 class="blogRollHeader">
        Blog Roll</h3>
    <div class="blogRollContent">
        <%= GetBlogRoll() %>
    </div>
</div>
