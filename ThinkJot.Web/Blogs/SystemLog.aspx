<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemLog.aspx.cs" Inherits="Blogs_SystemLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>System Error Log</title>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "prototype.js")) %>
    <%= GetJSLink(PathUtility.CombineUrls(SysConfig.ScriptsUrl, "blog.js")) %>
    <%= GetJSLink(GetCalendarPopupPath())%>
    <%= GetCssLink(PathUtility.CombineUrls(SysConfig.StyleUrl, "blog.css"))%>
    <%= GetCssLink(PathUtility.CombineUrls(SysConfig.StyleUrl, "blog-system.css"))%>
    <script language="javascript" type="text/javascript">
        var cal = new CalendarPopup();
    </script>

    <style type="text/css">
        .logEventHeader { padding:4px; background-color:#DDDDDD;}
        .logEventData { padding:8px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="systemControlPanelHeader">
                <h2>
                    System Error Log
                </h2>
            </div>
            <div>
                <input id="dateBox" name="dateBox" type="text" value="<%= GetDateText() %>" />
                <a href="#" onclick="cal.select(document.forms[0].dateBox,'dateAnchor','yyyy/MM/dd'); return false;"
                    id="dateAnchor">Change</a> (yyyy/MM/dd)</div>
            <br />
            <asp:Button ID="refreshButton" runat="server" Text="Display Results" OnClick="refreshButton_Click" />
            <br />
            <br />
            <%= GetLogAsHtml() %>
        </div>
    </form>
</body>
</html>
