using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Utils;

public partial class Blogs_ViewSearchEngineActivityControl : BlogControlBase
{
    DateTime dt;
    ActivityStats stats;
        
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDate();
        stats = ActivityLog.GetActivityStats(dt, BlogName);
    }

    protected string GetCalendarPopupPath()
    {
        return PathUtility.CombineUrls(SysConfig.ComponentsUrl, "calendarpopup/calendarpopup.js");
    }

    protected string GetDateText()
    {
        return dt.ToString("yyyy/MM/dd");
    }

    protected string GetTopSearchEnginesHtml()
    {
        List<ActivityStats.SearchEngineSummaryItem> searchEngines = stats.SearchEngineSummaryItems;
        string html = "<div class=\"activityStatsItem\"><table>";
        foreach (ActivityStats.SearchEngineSummaryItem item in searchEngines)
        {
            html += "<tr>";
            html += "<td class=\"activityStatsCol\"><a href=\"" + HttpUtility.HtmlEncode(item.SearchEngineUrl) + "\">" + item.SearchEngineName + "</a></td>";
            html += "<td class=\"activityStatsCol\">" + item.Count + "</td>";
            html += "</tr>";
        }        
        html += "</table></div>";
        return html;
    }

    protected string GetTopSearchQueriesHtml()
    {
        List<ActivityStats.SearchQueriesSummaryItem> queries = stats.SearchQueriesSummaryItems;
        string html = "<div class=\"activityStatsItem\"><table>";
        foreach (ActivityStats.SearchQueriesSummaryItem item in queries)
        {
            string query = (item.Query.Length <= 100) ? item.Query : item.Query.Substring(0, 100);            
            html += "<tr>";
            html += "<td class=\"activityStatsCol\">" + item.Count + "</td>";
            html += "<td class=\"activityStatsCol\"><a href=\"" + HttpUtility.HtmlEncode(item.Referrer) + "\">" + HttpUtility.HtmlEncode(query) + "</a> (" + item.SearchEngine + ")</td>";
            html += "</tr>";
        }
        html += "</table></div>";
        return html;
    }

    protected void refreshButton_Click(object sender, EventArgs e)
    {
        //This does nothing ;) -- just posts back
    }

    private void LoadDate()
    {
        string strDate = Request["dateBox"];
        dt = DateTimeUtility.GetDateFromShortDateString(strDate);
    }
}
