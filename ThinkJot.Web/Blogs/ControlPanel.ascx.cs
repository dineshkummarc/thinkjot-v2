/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;

public partial class Blogs_ControlPanel : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected string GetIcon(string which)
    {
        switch (which)
        {
            case "ManageContent":
                return PathUtility.CombineUrls(SysConfig.ImagesUrl, "Blogs/manage-content-ico.jpg");
            case "BlogSettings":
                return PathUtility.CombineUrls(SysConfig.ImagesUrl, "Blogs/blog-settings-ico.jpg");
            case "Activity":
                return PathUtility.CombineUrls(SysConfig.ImagesUrl, "Blogs/activity-ico.jpg");
            case "Security":
                return PathUtility.CombineUrls(SysConfig.ImagesUrl, "Blogs/security-ico.jpg");
            case "SystemSettings":
                return PathUtility.CombineUrls(SysConfig.ImagesUrl, "Blogs/system-settings-ico.jpg");
        }
        return "";
    }

    protected string GetLink(string which)
    {
        switch (which)
        {
            case "NewPost":
                return "adminpage.aspx?function=AddEntry";
            case "EditBlogRoll":
                return "adminpage.aspx?function=DirectTextEdit&amp;settingName=BlogRoll";
            case "EditBlogConfig":
                return "adminpage.aspx?function=DirectTextEdit&amp;settingName=BlogConfig";
            case "EditWebLinks":
                return "adminpage.aspx?function=DirectTextEdit&amp;settingName=WebLinks";
            case "EditCategories":
                return "adminpage.aspx?function=DirectTextEdit&amp;settingName=Categories";
            case "ManagePosts":
                return "adminpage.aspx?function=ManagePosts";
            case "ManageFiles":
                return "adminpage.aspx?function=ManageFiles";
            case "ManageTrash":
                return "adminpage.aspx?function=ManageTrash";
            case "MigrateDasBlog":
                return "adminpage.aspx?function=MigrateDasBlog";
            case "ReloadBlogConfig":
                return "adminpage.aspx?function=ReloadBlogConfig";
            case "ViewSearchEngineActivity":
                return "adminpage.aspx?function=ViewSearchEngineActivity";
            case "ViewReferrers":
                return "adminpage.aspx?function=ViewReferrers";
            case "ViewTopPagesAndUserAgents":
                return "adminpage.aspx?function=ViewTopPagesAndUserAgents";
            case "ViewVisitors":
                return "adminpage.aspx?function=ViewVisitors";
            case "ViewEvents":
                return "adminpage.aspx?function=ViewEvents";
            case "BlogConfigCommon":
                return "adminpage.aspx?function=BlogConfigCommon";
            case "UserManagement":
                return "adminpage.aspx?function=UserManagement";
            default:
                return "";
        }
    }
}
