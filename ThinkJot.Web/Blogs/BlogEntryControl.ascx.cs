/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Membership;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs;

public partial class Blogs_BlogEntryControl : BlogControlBase, IBlogEntryControl
{
    bool isCommentListEmpty = true;
    BlogEntry entry;
    public void LoadEntry(BlogEntry entry, bool minimalDisplay)
    {
        if (minimalDisplay)
        {
            strCommentsLabel.Visible = false;
        }

        this.entry = entry;

        //we will showComments if function == "DisplayItem";
        //  ie, when a single entry is displayed.
        bool showComments = false;
        string function = Request["function"];
        if (!string.IsNullOrEmpty(function) && function == "DisplayItem") showComments = true;
        else showComments = false;

        //is there an explicit value set? (for showComments)
        string strShowComments = Request["showComments"];
        if (!string.IsNullOrEmpty(strShowComments))
            showComments = Convert.ToBoolean(strShowComments.ToLower());

        string blogUrl = GetMappedBlogUrl();

        string dateText = "";
        DateTime displayTime;
        string timeZoneInfo = "";
    
        if (Config.UseUniversalTime)
        {
            displayTime = entry.Time;
            timeZoneInfo = "(UTC)";
        }
        else
        {
            if (Config.UseServerLocalTime)
            {
                displayTime = entry.Time.ToLocalTime();
                timeZoneInfo = "(UTC" + DateTimeUtility.GetUtcOffsetString(TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now.ToLocalTime())) + ")";
            }
            else
            {
                displayTime = DateTimeUtility.AddTimeOffset(entry.Time, Config.UtcOffset);
                timeZoneInfo += "<span class=\"blogEntryTimeZonePart\">" + " (UTC" + Config.UtcOffset + ")" + "</span>";
            }
        }

        dateText =  "<span class=\"blogEntryDatePart\">" + displayTime.ToLongDateString() + "</span>";
        dateText += "<span class=\"blogEntryTimePart\"> " + displayTime.ToShortTimeString() + "</span>";
        dateText += "<span class=\"blogEntryTimeZonePart\"> " + timeZoneInfo + "</span>";

        dateLabel.Text = dateText;

        //RepeatedSection:GetTitleForPost
        if (Config.UseTitleForLinks) //This means the Urls are created with title, instead of Guid
        {
            string strippedTitle = StringUtils.StripWhitespace_And_SpecialChars(entry.Title);

            if (entry.AllowComments)
            {
                string strComments = "";
                if (Config.ShowCommentCount) strComments = "Comments (" + entry.Comments.Count + ")";
                else strComments = "Comments";

                strCommentsLabel.Text = "<a href=\"" + PathUtility.CombineUrls(blogUrl, strippedTitle + ".aspx") +
                    "\">" + strComments + "</a>";
            }
            else
            {
                strCommentsLabel.Text = "";
                strCommentsLabel.Visible = false;
            }
            titleLabel.Text = "<a href=\"" + PathUtility.CombineUrls(blogUrl, strippedTitle + ".aspx") +
                "\">" + Server.HtmlEncode(entry.Title) + "</a>";
        }
        else
        {
            if (entry.AllowComments)
            {
                string strComments = "";
                if (Config.ShowCommentCount) strComments = "Comments (" + entry.Comments.Count + ")";
                else strComments = "Comments";

                strCommentsLabel.Text = "<a href=\"" + PathUtility.CombineUrls(blogUrl, "default.aspx?function=DisplayItem&amp;entryGuid=" + entry.UniqueId.ToString()) +
                    "\">" + strComments + "</a>";
            }
            else
            {
                strCommentsLabel.Text = "";
                strCommentsLabel.Visible = false;
            }
            titleLabel.Text = "<a href=\"" + PathUtility.CombineUrls(blogUrl, "default.aspx?function=DisplayItem&amp;entryGuid=" + entry.UniqueId.ToString()) +
                "\">" + Server.HtmlEncode(entry.Title) + "</a>";

        }

        if (showComments && entry.AllowComments)
        {            
            //show existing comments
            BlogControlBase commentControl;
            foreach (Comment comment in entry.Comments)
            {
                if (comment != null)
                {
                    commentControl = (BlogControlBase)GetOverridableControl("CommentControl.ascx");
                    ((ICommentControl)commentControl).LoadComment(comment);
                    commentsPanel.Controls.Add(commentControl);
                    isCommentListEmpty = false;
                }
            }

            if (!entry.CommentsClosed &&
                (entry.DisableCommentsAfterDays == -1 ||
                    (DateTime.Now < entry.Time.AddDays(entry.DisableCommentsAfterDays))))
            {
                //add new comments
                BlogControlBase commentEntryControl = (BlogControlBase)GetOverridableControl("CommentEntryControl.ascx");
                ((ICommentEntryControl)commentEntryControl).SetBlogEntryId(entry.UniqueId);
                commentEditPanel.Controls.Add(commentEntryControl);
            }
            else
            {
                commentsClosedLabel.Visible = true;
            }
        }
    }

    protected string GetCategoriesText()
    {
        if (Config.DisableCategories)
            return "";

        List<Category> categories = Category.GetCategories(entry.CategoryIds, BlogName);
        if (categories.Count == 0) return "";
        string result = "";
        result += "Posted under ";
        List<string> strCats = new List<string>();
        foreach (Category category in categories)
        {
            string strLink = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(), "default.aspx?function=ShowCategory&amp;categoryId="
                + category.Id) + "\">" + category.Name + "</a>";
            strCats.Add(strLink);
        }
        result += string.Join(" | ", strCats.ToArray());
        return result;
    }

    protected string GetTagsText()
    {
        if (entry.Tags.Count > 0)
        {
            string html = "Tags: ";
            List<string> strTags = new List<string>();
            foreach (string tag in entry.Tags)
            {
                if (!string.IsNullOrEmpty(tag))
                {
                    string strLink = "<a href=\"" + PathUtility.CombineUrls(GetMappedBlogUrl(), "default.aspx?function=FilterByTags&amp;tag="
                        + Server.UrlEncode(tag)) + "\">" + tag + "</a>";
                    strTags.Add(strLink);
                }
            }
            html += string.Join(" | ", strTags.ToArray());
            return html;
        }
        else return "";
    }

    protected string GetAuthor()
    {
        if (!string.IsNullOrEmpty(entry.Author))
        {
            return "by " + entry.Author;
        }
        return "";
    }

    protected string GetEntryEditText()
    {
        Guid entryId = entry.UniqueId;
        if (!new SecurityUtility().IsBlogEditor(BlogName)) return "";
        string text = "<p class=\"blogEntryEditLinks\"><a href=\"" + "adminpage.aspx?function=EditEntry&amp;entryId=" + entryId.ToString() + "\">Edit Post</a>" +
            " | <span class=\"linklessLink\" onclick=\"showDeleteBlogEntry('" + HtmlUtility.FormatJavascriptParams(entry.Title) + "', '" + entry.UniqueId + "', '" + BlogName + "', '" + Server.UrlEncode(Request.Url.AbsoluteUri) + "');\">Delete Post</span></p>";
        return text;
    }

    protected string GetContent()
    {
        return entry.Content;
    }

    protected bool HasComments()
    {
        return !isCommentListEmpty;
    }
}
