<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlogEntryControl.ascx.cs"
    Inherits="Blogs_BlogEntryControl" %>
<%@ Reference Control="~/Blogs/CommentControl.ascx" %>
<%@ Reference Control="~/Blogs/CommentEntryControl.ascx" %>
<div class="blogEntry post">
    <h4 class="dayHeader date">
        <asp:Label ID="dateLabel" runat="server" Text="Date"></asp:Label>
    </h4>
    <h2 class="blogEntryTitle title">
        <asp:Label ID="titleLabel" runat="server" Text="Title"></asp:Label>
    </h2>
    <div class="blogEntryAuthor author">
        <%= GetAuthor()%>
    </div>
    <div class="blogEntryContent content">
        <%= GetContent()%>
    </div>
    <div class="blogEntryTags tags">
        <%= GetTagsText()%>
    </div>
    <div class="blogEntryCategories categories">
        <%= GetCategoriesText()%>
    </div>
    <div class="commentsSection comments">
        <h3 class="blogEntryCommentsHeader">
            <asp:Label ID="strCommentsLabel" runat="server" Text="Comments"></asp:Label>
        </h3>
        <% if (HasComments()) { %>
        <ol class="blogEntryComments commentList">
            <asp:PlaceHolder ID="commentsPanel" runat="server"></asp:PlaceHolder>
        </ol>
        <% } %>
        <asp:PlaceHolder ID="commentEditPanel" runat="server"></asp:PlaceHolder>
    </div>
    <asp:Label ID="commentsClosedLabel" runat="server" Text="Comments are closed." CssClass="commentsClosedLabel"
        Visible="False"></asp:Label>
    <%= GetEntryEditText()%>
    <div class="blogEntryFooter">
    </div>
</div>
