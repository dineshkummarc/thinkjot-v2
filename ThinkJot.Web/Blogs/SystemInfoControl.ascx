<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SystemInfoControl.ascx.cs"
    Inherits="Blogs_SystemInfoControl" %>
<h2>
     <a href="<%= GetMappedBlogUrl() %>">&lt;&lt;Blog</a> | <a href="AdminPage.aspx?function=ControlPanel">
        Control Panel</a></h2>
<div class="systemInfoControl">
    <div class="systemInfoControlHeader">
        <h2>
            System Info
        </h2>
    </div>
    <div>
        <%= GetSystemInfo() %>
    </div>
</div>
