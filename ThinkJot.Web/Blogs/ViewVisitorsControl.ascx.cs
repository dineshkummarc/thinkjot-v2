using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Utils;

public partial class Blogs_ViewVisitorsControl : BlogControlBase
{
    DateTime dt;
    ActivityStats stats;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDate(); 
        stats = ActivityLog.GetActivityStats(dt, BlogName);
                
    }

    protected string GetCalendarPopupPath()
    {
        return PathUtility.CombineUrls(SysConfig.ComponentsUrl, "calendarpopup/calendarpopup.js");
    }

    protected string GetDateText()
    {
        return dt.ToString("yyyy/MM/dd");
    }

    protected string GetVisitorsHtml()
    {
        List<ActivityLogEntry> logEntries = stats.Entries;
        Dictionary<string, int> visitorVisitCount = new Dictionary<string, int>();
        foreach (ActivityLogEntry entry in logEntries)
        {
            if (visitorVisitCount.ContainsKey(entry.UserHostAddress))
                visitorVisitCount[entry.UserHostAddress]++;
            else
                visitorVisitCount[entry.UserHostAddress] = 1;
        }

        string html = "<div class=\"activityStatsItem\"><table>";
        foreach (KeyValuePair<string, int> pair in visitorVisitCount)
        {
            string visitorIP = pair.Key;
            if (string.IsNullOrEmpty(visitorIP.Trim()))
                visitorIP = "Unknown";
            html += "<tr>";
            html += "<td class=\"activityStatsCol\">" + pair.Value + "</td>";
            html += "<td class=\"activityStatsCol\">" +  visitorIP + "</td>";
            html += "</tr>";
        }
        html += "</table></div>";
        return html;
    }

    protected void refreshButton_Click(object sender, EventArgs e)
    {
        //This does nothing ;) -- just posts back
    }

    private void LoadDate()
    {
        string strDate = Request["dateBox"];
        dt = DateTimeUtility.GetDateFromShortDateString(strDate);
    }
}
