using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Blogs_SimpleTextDisplayControl : BlogControlBase, ISimpleTextDisplayControl
{
    string displayText = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void LoadText(string text)
    {
        this.displayText = text;
    }

    protected string GetDisplayText()
    {
        return displayText;
    }
}