/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;

public partial class Blogs_ManageFilesView : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    protected string GetMediaFilesUrl()
    {
        return PathUtility.CombineUrls(GetBlogUrl(), "Media");
    }

    protected void filesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow row = filesGrid.Rows[e.RowIndex];
        HiddenField fileNameField = (HiddenField)row.FindControl("fileNameHiddenField");
        MediaFile.DeleteFile(fileNameField.Value, BlogName);
        LoadData();
    }

    protected void filesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {            
            MediaFile mediaFile = (MediaFile)e.Row.DataItem;
            e.Row.Cells[0].Text = "<a href=\"" + PathUtility.CombineUrls(GetMediaFilesUrl(), mediaFile.FileName) +
                "\">" + mediaFile.FileName + "</a>";
        }
    }

    private void LoadData()
    {
        filesGrid.DataSource = MediaFile.GetMediaFiles(BlogName);
        filesGrid.DataBind();
    }
}