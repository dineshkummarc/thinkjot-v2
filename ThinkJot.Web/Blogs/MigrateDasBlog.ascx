<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MigrateDasBlog.ascx.cs"
    Inherits="Blogs_MigrateDasBlog" %>
<div class="migrateTextHeader">
    Steps</div>
1. Load the dasBlog "*.dayentry.xml" and "*.dayfeedback.xml" files under ~/App_Data/XmlData/&lt;blog_name&gt;/dasBlog_Content/<br />
2. Click the button below.<br />
&nbsp;<br />
<asp:Button ID="migrateButton" runat="server" OnClick="migrateButton_Click" Text="Migrate"
    Width="120px" />
