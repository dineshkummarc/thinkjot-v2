/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;

public partial class Blogs_SearchResultsView : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string keyWords = Request["keywords"];
        List<BlogEntry> blogEntries = BlogEntry.SearchBlogEntries(BlogName, keyWords.Split(' '));
        Blogs_BlogEntriesView view = (Blogs_BlogEntriesView)Page.LoadControl("~/Blogs/BlogEntriesView.ascx");
        view.ParseRequest = false;
        view.LoadEntries(blogEntries);
        entriesPanel.Controls.Add(view);
    }
}