<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonthControl.ascx.cs"
    Inherits="Blogs_MonthControl" %>
<%@ OutputCache Duration="120" VaryByParam="None" %>
<asp:Repeater ID="monthsRepeater" runat="server">
    <HeaderTemplate>
        <div class="monthViewBox">
            <h3>
                Months</h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li class="monthViewBoxEntry"><a href="<%# GetDisplayMonthLink(Container.DataItem) %>">
            <%# DataBinder.Eval(Container.DataItem,"DisplayString") %>
        </a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
