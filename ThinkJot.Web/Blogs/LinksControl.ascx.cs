/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Blogs;

public partial class Blogs_LinksControl : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WebLinks links = WebLinks.Load(BlogName);
        linksRepeater.DataSource = links.Entries;
        linksRepeater.DataBind();
    }
}
