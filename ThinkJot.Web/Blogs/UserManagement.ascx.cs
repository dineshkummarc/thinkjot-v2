using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs;

public partial class Blogs_UserManagement : BlogControlBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //This page should not be accessible if the user is not logged in.
        if (!new SecurityUtility().IsSystemAdministrator())
            Response.Redirect(SysConfig.SiteRoot);

        Users users = Users.Load(BlogName);
        usersRepeater.DataSource = users.Entries;
        usersRepeater.DataBind();
    }
}
