using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Utils;

public partial class Blogs_ViewEventsControl : BlogControlBase
{
    DateTime dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDate();
    }

    protected string GetCalendarPopupPath()
    {
        return PathUtility.CombineUrls(SysConfig.ComponentsUrl, "calendarpopup/calendarpopup.js");
    }

    protected string GetDateText()
    {
        return dt.ToString("yyyy/MM/dd");
    }

    protected string GetEventLogHtml()
    {
        string html = "";

        Logger logger = BlogDataProvider.GetLogger();
        string contents = logger.ReadLog(dt, BlogName);

        if (string.IsNullOrEmpty(contents))
            return "";

        StringReader reader = new StringReader(contents);
        string aLine;
        while (true)
        {
            aLine = reader.ReadLine();
            if (aLine != null)
            {
                //eg: ThinkJotLog_Event:ed59716e-9f02-4a5d-913f-81503eabf057; ThinkJotLog_Time:3/8/2007 9:48:54 AM
                if (aLine.StartsWith("ThinkJotLog_Event:"))
                {
                    string moddedLine = aLine.Replace("ThinkJotLog_Event:", "<strong>Event Id: </strong>").Replace("ThinkJotLog_Time:", "<strong>Time: </strong>");
                    html += "<div class=\"logEventHeader\">" + moddedLine + "</div>";
                }
                else
                    html += "<div class=\"logEventData\">" + aLine + "</div>";
            }
            else
                break;
        }
        return html;
    }

    protected void refreshButton_Click(object sender, EventArgs e)
    {
        //This does nothing ;) -- just posts back
    }
    private void LoadDate()
    {
        string strDate = Request["dateBox"];
        dt = DateTimeUtility.GetDateFromShortDateString(strDate);
    }
}
