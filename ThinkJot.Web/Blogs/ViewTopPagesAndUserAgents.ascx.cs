using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Utils;

public partial class Blogs_ViewTopPagesAndUserAgents : BlogControlBase
{
    DateTime dt;
    ActivityStats stats;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadDate();
        stats = ActivityLog.GetActivityStats(dt, BlogName);
    }

    protected string GetCalendarPopupPath()
    {
        return PathUtility.CombineUrls(SysConfig.ComponentsUrl, "calendarpopup/calendarpopup.js");
    }

    protected string GetDateText()
    {
        return dt.ToString("yyyy/MM/dd");
    }

    protected string GetTopPagesHtml()
    {
        List<ActivityStats.RequestedPagesSummaryItem> queries = stats.RequestedPagesSummaryItems;
        string html = "<div class=\"activityStatsItem\"><table>";
        foreach (ActivityStats.RequestedPagesSummaryItem item in queries)
        {
            html += "<tr>";
            html += "<td class=\"activityStatsCol\">" + item.Count + "</td>";
            html += "<td class=\"activityStatsCol\"><a href=\"" + HttpUtility.HtmlEncode(item.PageUrl) + "\">" + HttpUtility.HtmlEncode(item.PageUrl) + "</a></td>";
            html += "</tr>";
        }
        html += "</table></div>";
        return html;
    }

    protected string GetUserAgentsHtml()
    {
        List<ActivityStats.UserAgentSummaryItem> queries = stats.UserAgentSummaryItems;
        string html = "<div class=\"activityStatsItem\"><table>";
        foreach (ActivityStats.UserAgentSummaryItem item in queries)
        {
            html += "<tr>";
            html += "<td class=\"activityStatsCol\">" + item.Count + "</td>";
            html += "<td class=\"activityStatsCol\">" + HttpUtility.HtmlEncode(item.UserAgent) + "</td>";
            html += "</tr>";
        }
        html += "</table></div>";
        return html;
    }

    protected void refreshButton_Click(object sender, EventArgs e)
    {
        //This does nothing ;) -- just posts back
    }

    private void LoadDate()
    {
        string strDate = Request["dateBox"];
        dt = DateTimeUtility.GetDateFromShortDateString(strDate);
    }
}
