/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using ThinkJot.Core.Blogs;

public partial class Blogs_CommentEntryControl : BlogControlBase, ICommentEntryControl
{
    private delegate void AssignValue();

    protected void Page_Load(object sender, EventArgs e)
    {
        errorLabel.Visible = false;
        if (!Page.IsPostBack)
        {
            if (Config.UseCaptchaForComments)
            {
                Session["CaptchaImageText"] = GenerateRandomCode();              
            }
        }
    }

    public void SetBlogEntryId(Guid entryId)
    {
        ViewState["entryId"] = entryId;
    }

    protected void saveCommentButton_Click(object sender, EventArgs e)
    {
        if (Config.UseCaptchaForComments)
        {
            if (captchaTextBox.Text != (string)Session["CaptchaImageText"])
            {
                //So that no one uses the Captcha Again
                Session["CaptchaImageText"] = GenerateRandomCode();
                ShowError("Please enter the validation correctly.");
                return;
            }
        }

        Guid entryId;
        if (ViewState["entryId"] != null)
        {
            entryId = (Guid)ViewState["entryId"];
        }
        else return;
        BlogEntry entry = BlogEntry.GetBlogEntry(entryId, BlogName);
        
        //re-check required.
        if (!entry.AllowComments || entry.CommentsClosed) return;

        Comment comment = new Comment();
        comment.Time = DateTime.Now.ToUniversalTime();
        string errorField = "";
        try
        {
            comment.Author = ValidateAndGetValue(authorTextBox.Text, 50, "Name", ref errorField);
            comment.AuthorEmail = ValidateAndGetValue(emailTextBox.Text, 60, "Email", ref errorField);
            comment.AuthorHomepage = ValidateAndGetValue(homepageTextBox.Text, 120, "HomePage", ref errorField);
            comment.Text = ValidateAndGetValue(commentTextBox.Text, 1000, "Comment", ref errorField);
            comment.TargetEntryId = entryId;
            comment.ExternalLink = ValidateAndGetValue(detailsUrlTextBox.Text, 120, "Details Url", ref errorField);
            comment.AuthorIpAddress = ValidateAndGetValue(Request.UserHostAddress, 20, "IpAddress", ref errorField);
        }
        catch
        {
            ShowError(errorField + " exceeds maximum length.");
            return;
        }
        Comment.SaveComment(comment, BlogName);
        Response.Redirect(Request.RawUrl);
    }

    //
    // Returns a string of six random digits.
    //
    private string GenerateRandomCode()
    {
        Random random = new Random();
        string s = "";
        for (int i = 0; i < 5; i++)
            s = String.Concat(s, random.Next(10).ToString());
        return s;
    }

    private string ValidateAndGetValue(string what, int maxLength, string fieldName, ref string errorField)
    {
        if (what.Length < maxLength) return what;
        else 
        {
            errorField = fieldName;
            throw new Exception();
        }
    }

    private void ShowError(string errorText)
    {
        errorLabel.Visible = true;
        errorLabel.Text = errorText;
    }
}