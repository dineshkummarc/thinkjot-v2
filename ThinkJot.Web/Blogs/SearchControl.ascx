<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchControl.ascx.cs" Inherits="Blogs_SearchControl" %>
<div id="search">
    <asp:TextBox ID="searchTextBox" runat="server"></asp:TextBox>
    <asp:Button ID="searchButton" runat="server" Text="Search" OnClick="searchButton_Click" CausesValidation="False" /></div>
   