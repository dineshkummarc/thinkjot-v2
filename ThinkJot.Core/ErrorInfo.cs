/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core
{
    public class ErrorInfo
    {
        public ErrorInfo()
        {
            ErrorCode = 0;
            Message = "";
        }
        public int ErrorCode;
        public string Message;

        public bool HasErrors
        {
            get
            {
                if (ErrorCode != 0) return true;
                else return false;
            }
        }
    }
}
