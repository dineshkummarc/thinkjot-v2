using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.UrlMapping
{
    public interface IUrlMapEntry
    {
        string Url { get; set;  }
        List<string> UrlMapExtensions { get; set; }
    }
}
