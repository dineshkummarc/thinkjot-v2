using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.UrlMapping
{
    public class BlogUrlMapEntry : IUrlMapEntry
    {
        string blogName;
        public string BlogName
        {
            get { return blogName; }
            set { blogName = value; }
        }

        string url;
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        private List<string> urlMapExtensions;
        public List<string> UrlMapExtensions
        {
            get { return urlMapExtensions; }
            set { urlMapExtensions = value; }
        }

        public BlogUrlMapEntry(string blogName, string url, List<string> extensions)
        {
            this.blogName = blogName;
            this.url = url;
            this.urlMapExtensions = extensions;
        }
    }
}
