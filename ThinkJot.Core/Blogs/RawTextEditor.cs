/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class RawTextEditor
    {
        public static string GetRawSettingsText(string settingName, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            return textEditorDP.GetRawSettingsText(settingName, blogName);
        }

        public static void PersistRawSettingsText(string settingName, string newValue, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            textEditorDP.PersistRawSettingsText(settingName, newValue, blogName);
        }

        public static string GetRawBlogEntryText(Guid entryId, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            return textEditorDP.GetRawBlogEntryText(entryId, blogName);
        }

        public static void PersistRawBlogEntryText(Guid entryId, string newText, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            textEditorDP.PersistRawBlogEntryText(entryId, newText, blogName);
        }

        public static string GetRawBlogCommentsText(Guid entryId, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            return textEditorDP.GetRawBlogCommentsText(entryId, blogName);
        }

        public static void PersistRawBlogCommentsText(Guid entryId, string newText, string blogName)
        {
            RawTextEditorDP textEditorDP = BlogDataProvider.GetRawTextEditorDP();
            textEditorDP.PersistRawBlogCommentsText(entryId, newText, blogName);
        }

        private string ReadFile(string fileName, string blogName)
        {
            return "";
        }
    }
}
