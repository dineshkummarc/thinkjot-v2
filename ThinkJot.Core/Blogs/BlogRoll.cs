/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class BlogRoll
    {
        List<BlogRollEntry> entries = new List<BlogRollEntry>();
        public List<BlogRollEntry> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public static string GetFormattedBlogRollContent(string blogName)
        {
            BlogRollDP provider = BlogDataProvider.GetBlogRollProvider();
            return provider.GetFormattedBlogRollContent(blogName);
        }

        public static BlogRoll GetBlogRoll(string blogName)
        {
            BlogRollDP provider = BlogDataProvider.GetBlogRollProvider();
            return provider.GetBlogRoll(blogName);
        }
    }
}
