/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class BlogRollDP
    {
        public abstract string GetFormattedBlogRollContent(string blogName);
        public abstract BlogRoll GetBlogRoll(string blogName);
    }
}