/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class BlogEntryDP
    {
        public abstract BlogEntry GetBlogEntry(Guid guid, string blogName);
        public abstract void SaveBlogEntry(BlogEntry entry, string blogName);
        public abstract void UpdateBlogEntry(BlogEntry entry, string blogName);
        public abstract void DeleteBlogEntry(Guid guid, string blogName);
        public abstract List<BlogEntry> GetAllBlogEntries(string blogName);
        public abstract List<BlogEntry> GetAllBlogEntriesAfter(DateTime when, string blogName);
        public abstract List<BlogEntry> SearchBlogEntries(string blogName, string[] searchStrings);
        public abstract List<BlogEntry> GetLastNormalTypeEntries(int count, bool showUnpublished, string blogName);
        public abstract string SaveFile(string fileName, byte[] fileBytes, string blogName, bool overwrite);
        public abstract BlogEntry GetBlogEntryByTitle(string title, string blogName);
        public abstract List<BlogEntry> GetEntriesForMonth(int month, int year, string blogName);
        public abstract List<BlogEntry> GetEntriesInCategory(int categoryId, int start, int count, out int totalItems, string blogName);
        public abstract List<BlogEntry> GetEntriesByTag(string tag, int start, int count, out int totalItems, string blogName);
        public abstract List<BlogEntrySummary> GetLastEntriesSummary(int count, string blogName);
        public abstract List<BlogEntry> GetBlogEntries(List<Guid> ids, string blogName);
        public abstract List<BlogEntry> GetDeletedEntries(string blogName);
        public abstract void Undelete(Guid entryId, string blogName);
        public abstract BlogEntry GetDeletedBlogEntry(Guid blogEntryId, string blogName);
        public abstract Guid GetEntryIdForTitle(string title, string blogName);
    }
}
