/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;

using Rss;
using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class RSSFeedDP
    {
        public void UpdateFeeds(string blogName)
        {
            try
            {
                BlogConfig config = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);

                int numItems = config.NumItemsOnFeed;
                //Get the last numItems feeds
                XmlBlogEntryDP entryDP = new XmlBlogEntryDP();
                List<BlogEntry> blogEntries = entryDP.GetLastNormalTypeEntries(numItems, false, blogName);
                if (blogEntries.Count == 0) return; //dont do anything if there are no items

                string title = config.BlogTitle;
                string fileName = Utils.GetRssFeedFileName();
                string filePath = Utils.GetRssFeedFilePath(blogName);

                // Create and write an RSS feed
                RssFeed newFeed = new RssFeed();

                // Create channel and set its properties
                RssChannel newChannel = new RssChannel();
                newChannel.Title = HttpUtility.HtmlEncode(title);
                newChannel.Description = "RSS feed for " + HttpUtility.HtmlEncode(title);
                newChannel.LastBuildDate = DateTime.Now.ToUniversalTime();
                newChannel.PubDate = DateTime.Now.ToUniversalTime();
                newChannel.Link = new Uri(new PathUtility().CombineUrls(new PathUtility().GetMappedBlogUrl(blogName), fileName));
                if (!string.IsNullOrEmpty(config.Contact)) newChannel.ManagingEditor = HttpUtility.HtmlEncode(config.Contact);
                if (!string.IsNullOrEmpty(config.Copyright)) newChannel.Copyright = HttpUtility.HtmlEncode(config.Copyright);
                if (!!string.IsNullOrEmpty(config.ContactEmail)) newChannel.WebMaster = HttpUtility.HtmlEncode(config.ContactEmail);

                bool hasEntries = false;
                foreach (BlogEntry entry in blogEntries)
                {
                    if (!entry.Publish || !entry.Syndicate) continue;
                    hasEntries = true; //We have atleast one entry.
                    // Create first item and set its properties
                    RssItem newItem = new RssItem();
                    newItem.Title = HttpUtility.HtmlEncode(entry.Title);
                    if (!string.IsNullOrEmpty(entry.Description)) newItem.Description = entry.Description;
                    else newItem.Description = entry.Content;
                    newItem.PubDate = entry.Time;

                    PathUtility pathUtil = new PathUtility();

                    if (config.UseTitleForLinks)
                        newItem.Link = new Uri(pathUtil.CombineUrls(pathUtil.GetMappedBlogUrl(blogName), StringUtils.StripWhitespace_And_SpecialChars(entry.Title) + ".aspx"));
                    else
                        newItem.Link = new Uri(pathUtil.CombineUrls(pathUtil.GetMappedBlogUrl(blogName), "default.aspx?function=DisplayItem&amp;entryGuid=" + entry.UniqueId.ToString()));

                    newChannel.Items.Add(newItem);
                }

                // add the channel to the feed
                newFeed.Channels.Add(newChannel);

                if (hasEntries) newFeed.Write(filePath);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }
    }
}
