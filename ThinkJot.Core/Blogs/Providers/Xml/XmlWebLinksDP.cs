/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlWebLinksDP : WebLinksDP
    {
        public override void SaveWebLinks(WebLinks links, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override WebLinks GetWebLinks(string blogName)
        {
            try
            {
                string linksPath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), "weblinks.xml");
                return (WebLinks)new SerializationHelper().Deserialize(linksPath, typeof(WebLinks), ProviderSettings.NamespaceURI);
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new WebLinks();
            }
        }
    }
}