/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlTemplateMapDP : TemplateMapDP
    {
        private const string TemplateMapFileName = "templatemap.xml";

        public override void RemoveEntry(Guid entryId, string blogName)
        {
            try
            {
                SerializationHelper serializer = new SerializationHelper();
                string templateMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TemplateMapFileName);
                TemplateMap templateMap = (TemplateMap)serializer.Deserialize(templateMapFilePath, typeof(TemplateMap), ProviderSettings.NamespaceURI);

                templateMap.Entries.RemoveAll(
                    delegate(TemplateMapEntry entry)
                    {
                        if (entry.EntryId == entryId) return true;
                        else return false;
                    });
                serializer.Serialize(templateMap, TemplateMapFileName, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public override void AddEntry(Guid entryId, string template, string blogName)
        {
            try
            {

                SerializationHelper serializer = new SerializationHelper();
                string templateMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TemplateMapFileName);
                TemplateMap templateMap = (TemplateMap)serializer.Deserialize(templateMapFilePath, typeof(TemplateMap), ProviderSettings.NamespaceURI);

                TemplateMapEntry entry = new TemplateMapEntry();
                entry.EntryId = entryId;
                entry.Template = template;
                templateMap.Entries.Add(entry);
                serializer.Serialize(templateMap, TemplateMapFileName, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public override void RemoveTemplate(string template, string blogName)
        {
            try
            {

                SerializationHelper serializer = new SerializationHelper();
                string templateMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TemplateMapFileName);
                TemplateMap templateMap = (TemplateMap)serializer.Deserialize(templateMapFilePath, typeof(TemplateMap), ProviderSettings.NamespaceURI);

                templateMap.Entries.RemoveAll(
                    delegate(TemplateMapEntry entry)
                    {
                        if (entry.Template == template) return true;
                        else return false;
                    });
                serializer.Serialize(templateMap, TemplateMapFileName, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
                
        }

        public override string GetTemplateForEntry(Guid entryId, string blogName)
        {
            try
            {

                SerializationHelper serializer = new SerializationHelper();
                string templateMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TemplateMapFileName);
                TemplateMap templateMap = (TemplateMap)serializer.Deserialize(templateMapFilePath, typeof(TemplateMap), ProviderSettings.NamespaceURI);

                foreach (TemplateMapEntry entry in templateMap.Entries)
                {
                    if (entry.EntryId == entryId) return entry.Template;
                }
                return null;
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return null;
            }
        }
    }
}
