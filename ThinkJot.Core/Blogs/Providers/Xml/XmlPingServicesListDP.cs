/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlPingServicesListDP : PingServicesListDP
    {
        public override PingServicesList GetPingServicesList(string blogName)
        {
            try
            {
                string path = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), "pingserviceslist.xml");
                return (PingServicesList)new SerializationHelper().Deserialize(path, typeof(PingServicesList), ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new PingServicesList();
            }
        }
    }
}
