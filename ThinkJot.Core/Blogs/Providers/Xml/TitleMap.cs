/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class TitleMapEntry
    {
        private Guid entryId;
        public Guid EntryId
        {
            get { return entryId; }
            set { entryId = value; }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
    }

    public class TitleMap
    {
        private const string TitleMapFileName = "titlemap.xml";

        List<TitleMapEntry> entries = new List<TitleMapEntry>();
        public List<TitleMapEntry> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public void Add(string title, Guid entryId)
        {
            TitleMapEntry entry = new TitleMapEntry();
            entry.EntryId = entryId;
            entry.Title = title;
            entries.Add(entry);
        }

        public void Remove(Guid entryId)
        {
            entries.RemoveAll(
                delegate(TitleMapEntry entry)
                {
                    if (entry.EntryId == entryId) return true;
                    else return false;
                });
        }
   
        public Guid GetEntryId(string title)
        {
            string titleHackedOff = StringUtils.StripWhitespace_And_SpecialChars(title.ToLower());
            foreach (TitleMapEntry entry in entries)
            {
                if (StringUtils.StripWhitespace_And_SpecialChars(entry.Title.ToLower()) == titleHackedOff) return entry.EntryId;
            }
            return Guid.Empty;
        }

        public void Save(string blogName)
        {
            try
            {
                SerializationHelper serializer = new SerializationHelper();
                string titleMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TitleMapFileName);
                serializer.Serialize(this, titleMapFilePath, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public static TitleMap GetTitleMap(string blogName)
        {
            try
            {
                SerializationHelper serializer = new SerializationHelper();
                string titleMapFilePath = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), TitleMapFileName);
                TitleMap titleMap = (TitleMap)serializer.Deserialize(titleMapFilePath, typeof(TitleMap), ProviderSettings.NamespaceURI);
                return titleMap ?? new TitleMap();
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new TitleMap();
            }
        }
        
        public void AddEntry(BlogEntry entry)
        {
            Add(entry.Title, entry.UniqueId);
        }
    }
}
