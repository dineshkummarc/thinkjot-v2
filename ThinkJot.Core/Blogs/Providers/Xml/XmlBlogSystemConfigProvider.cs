/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlBlogSystemConfigProvider : BlogSystemConfigProvider
    {
        static Dictionary<string, BlogConfig> blogConfigs = new Dictionary<string, BlogConfig>();
        
        static XmlBlogSystemConfigProvider()
        {
            LoadConfigs();
        }

        public override BlogConfig GetBlogConfig(string blogName)
        {
            return blogConfigs[blogName.ToLower()];
        }

        public override Dictionary<string, BlogConfig> GetAll()
        {
            return blogConfigs;
        }

        public override void ReloadBlogConfig(string blogName)
        {
            //no try catch here
            //since we want the user to be able to see the de-serialization errors
            string path = Path.Combine(ProviderSettings.GetBlogSettingsFolder(blogName), "blog.config");
            BlogConfig blogConfig = (BlogConfig)new SerializationHelper().Deserialize(path, typeof(BlogConfig), ProviderSettings.NamespaceURI);
            
            //If something failed, we would not get here.
            blogConfigs[blogName] = blogConfig;
        }

        private static void LoadConfigs()
        {
            try
            {
                string[] blogDirs = Directory.GetDirectories(ProviderSettings.GetDataFolder());
                
                BlogConfig blogConfig;
                foreach (string directory in blogDirs)
                {
                    //if a blog-config file has errors, we skip loading it.
                    try
                    {
                        //foldername is the blogname
                        string[] folders = directory.Split(Path.DirectorySeparatorChar);
                        string folderName = folders[folders.Length - 1].ToLower();

                        //two rules... cannot start with "." or "tj_system"
                        if (folderName.StartsWith(".") || folderName.StartsWith("tj_system"))
                            continue;

                        blogConfig = (BlogConfig)new SerializationHelper().Deserialize(Path.Combine(directory, "blog.config"), typeof(BlogConfig), ProviderSettings.NamespaceURI);
                        if (blogConfig != null)
                        {
                            blogConfig.Name = folderName; //whatever is there in xml, we overwrite!
                            blogConfigs.Add(folderName, blogConfig);                            
                        }
                    }
                    catch (Exception ex)
                    {
                        //BlogConfigs arent initialized yet
                        //So log as system exception
                        new XmlLogger().LogSystemException(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                new XmlLogger().LogSystemException(ex);
                //no option here, but to rethrow
                throw;
            }
        }
    }
}