/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlMonthViewDP : MonthViewDP
    {
        public override MonthView GetMonthView(string blogName)
        {
            try
            {
                Dictionary<int, int> monthData = new Dictionary<int, int>();
                DateTimeMap map = DateTimeMap.GetDateTimeMap(blogName);
                if (map == null) return new MonthView();
                foreach (DateTimeMapEntry entry in map.Entries)
                {
                    int monthNum = entry.Time.Year * 12 + entry.Time.Month;
                    if (!monthData.ContainsKey(monthNum)) monthData[monthNum] = 1;
                    else monthData[monthNum]++;
                }

                MonthView monthView = new MonthView();
                foreach (KeyValuePair<int, int> pair in monthData)
                {
                    int month = pair.Key % 12;
                    int year = pair.Key / 12;
                    if (month == 0)
                    {
                        month = 12;
                        year--;
                    }
                    monthView.Entries.Add(
                        new MonthViewEntry(month, year, pair.Value));
                }
                monthView.Entries.Reverse();
                return monthView;
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new MonthView();
            }
        }
    }
}
