/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public static class ProviderSettings
    {
        public const string NamespaceURI = "urn:process64-com:thinkjot:data";
        public static string GetBlogContentFolder(string blogName)
        {
            return Path.Combine(SystemConfiguration.GetConfig().AppRoot, Path.Combine(GetDataFolder(), blogName));
        }

        public static string GetBlogSettingsFolder(string blogName)
        {
            return Path.Combine(SystemConfiguration.GetConfig().AppRoot, Path.Combine(GetDataFolder(), blogName));
        }

        public static string GetDataFolder()
        {
            return Path.Combine(SystemConfiguration.GetConfig().AppRoot, Path.Combine("App_Data", Path.Combine("Blogs", "XmlData")));
        }

        public static string GetDeletedPostsFolder(string blogName)
        {
            return Path.Combine(GetBlogContentFolder(blogName), "Deleted");
        }

        public static string GetUsersConfigFolder()
        {
            return Path.Combine(SystemConfiguration.GetConfig().AppRoot, "App_Data");
        }
    }
}
