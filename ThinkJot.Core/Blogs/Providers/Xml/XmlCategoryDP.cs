/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlCategoryDP : CategoryDP
    {
        public override void AddCategory(Category category, string blogName)
        {
            try
            {
                List<Category> categories = (List<Category>)new SerializationHelper().Deserialize(GetCategoryFilename(blogName), typeof(List<Category>), ProviderSettings.NamespaceURI);
                category.Id = categories.Count + 1; //ignore the incoming categoryId. We will use our own.
                categories.Add(category);
                new SerializationHelper().Serialize(categories, GetCategoryFilename(blogName), ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public override void DeleteCategory(string categoryName, string blogName)
        {
            try
            {
                List<Category> categories = (List<Category>)new SerializationHelper().Deserialize(GetCategoryFilename(blogName), typeof(List<Category>), ProviderSettings.NamespaceURI);
                categories.RemoveAll(delegate(Category category)
                {
                    return category.Name == categoryName;
                });
                new SerializationHelper().Serialize(categories, GetCategoryFilename(blogName), ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public override Category GetCategory(int id, string blogName)
        {
            try
            {
                List<Category> categories = (List<Category>)new SerializationHelper().Deserialize(GetCategoryFilename(blogName), typeof(List<Category>), ProviderSettings.NamespaceURI);
                foreach (Category category in categories)
                {
                    if (category.Id == id)
                    {
                        return category;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return null;
            }
        }

        public override List<Category> GetCategories(List<int> ids, string blogName)
        {
            try
            {
                List<Category> results = new List<Category>();
                List<Category> categories = (List<Category>)new SerializationHelper().Deserialize(GetCategoryFilename(blogName), typeof(List<Category>), ProviderSettings.NamespaceURI);
                foreach (Category category in categories)
                {
                    if (ids.Contains(category.Id))
                    {
                        results.Add(category);
                    }
                }
                return results;
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new List<Category>();
            }
        }

        public override List<Category> GetAllCategories(string blogName)
        {
            try
            {
                List<Category> categories = (List<Category>)new SerializationHelper().Deserialize(GetCategoryFilename(blogName), typeof(List<Category>), ProviderSettings.NamespaceURI);
                return categories;
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new List<Category>();
            }
        }

        private string GetCategoryFilename(string blogName)
        {
            return Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), "categories.xml");
        }        
    }
}
