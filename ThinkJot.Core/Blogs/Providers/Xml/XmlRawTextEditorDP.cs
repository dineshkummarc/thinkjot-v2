/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlRawTextEditorDP : RawTextEditorDP
    {
        public override string GetRawSettingsText(string settingName, string blogName)
        {
            switch (settingName)
            {
                case "BlogRoll":
                    return ReadSettingsFile("blogroll.opml", blogName);
                case "WebLinks":
                    return ReadSettingsFile("weblinks.xml", blogName);
                case "BlogConfig":
                    return ReadSettingsFile("blog.config", blogName);
                case "Categories":
                    return ReadSettingsFile("categories.xml", blogName);
            }
            return null;
        }

        public override void PersistRawSettingsText(string settingName, string newText, string blogName)
        {
            switch (settingName)
            {
                case "BlogRoll":
                    WriteSettingsFile("blogroll.opml", newText, blogName);
                    break;
                case "WebLinks":
                    WriteSettingsFile("weblinks.xml", newText, blogName);
                    break;
                case "BlogConfig":
                    WriteSettingsFile("blog.config", newText, blogName);
                    break;
                case "Categories":
                    WriteSettingsFile("categories.xml", newText, blogName);
                    break;
            }
        }

        public override string GetRawBlogEntryText(Guid entryId, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void PersistRawBlogEntryText(Guid entryId, string newText, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string GetRawBlogCommentsText(Guid entryId, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void PersistRawBlogCommentsText(Guid entryId, string newText, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private string ReadSettingsFile(string fileName, string blogName)
        {
            return ReadFile(fileName, "settings", blogName);
        }

        private string ReadContentFile(string fileName, string blogName)
        {
            return ReadFile(fileName, "contents", blogName);
        }

        private string ReadFile(string fileName, string fileType, string blogName)
        {
            string path = "";
            if (fileType == "settings")
                path = Path.Combine(ProviderSettings.GetBlogSettingsFolder(blogName), fileName);
            if (fileType == "contents")
                path = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), fileName);

            if (File.Exists(path))
                return File.ReadAllText(path);

            return null;
        }

        private void WriteSettingsFile(string fileName, string content, string blogName)
        {
            WriteFile(fileName, "settings", content, blogName);
        }

        private void WriteContentFile(string fileName, string content, string blogName)
        {
            WriteFile(fileName, "contents", content, blogName);
        }

        private void WriteFile(string fileName, string fileType, string content, string blogName)
        {
            string path = "";
            if (fileType == "settings")
                path = Path.Combine(ProviderSettings.GetBlogSettingsFolder(blogName), fileName);
            if (fileType == "contents")
                path = Path.Combine(ProviderSettings.GetBlogContentFolder(blogName), fileName); 
            
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.Write(content);
            }

            //Flush cache in SerializationManager due to the direct edit.
            //ugly?
            SerializationManager.RemoveObjectFromCache(path);

        }
    }
}