/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlFeedsDP : FeedsDP
    {
        public override void UpdateFeeds(string blogName)
        {
            try
            {
                RSSFeedDP rssDP = new RSSFeedDP();
                rssDP.UpdateFeeds(blogName);
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }
    }
}
