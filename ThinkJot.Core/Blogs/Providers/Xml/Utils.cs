/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;

using ThinkJot.Core.Configuration;
using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public static class Utils
    {
        //? Do we have date time format issues here?
        public static string GetDate(DateTime dateTime)
        {
            return dateTime.ToShortDateString().Replace('/', '-');
        }

        public static string GetEntryFileName(Guid entryId)
        {
            return entryId.ToString() + ".entry.xml";
        }

        public static string GetEntryFilePath(Guid entryId, string blogName)
        {
            string dataFolder = ProviderSettings.GetBlogContentFolder(blogName);
            return Path.Combine(dataFolder, GetEntryFileName(entryId));
        }

        public static string GetDeletedEntryFilePath(Guid entryId, string blogName)
        {
            string dataFolder = ProviderSettings.GetDeletedPostsFolder(blogName);
            return Path.Combine(dataFolder, GetEntryFileName(entryId));
        }

        public static string GetCommentFileName(Guid entryId)
        {
            return entryId.ToString() + ".comments.xml";
        }

        public static string GetCommentFilePath(Guid entryId, string blogName)
        {
            string dataFolder = ProviderSettings.GetBlogContentFolder(blogName);
            return Path.Combine(dataFolder, GetCommentFileName(entryId));
        }

        public static string GetDeletedCommentFilePath(Guid entryId, string blogName)
        {
            string dataFolder = ProviderSettings.GetDeletedPostsFolder(blogName);
            return Path.Combine(dataFolder, GetCommentFileName(entryId));
        }

        public static string GetRssFeedFileName()
        {
            return "rss.xml";
        }

        public static string GetRssFeedFilePath(string blogName)
        {
            string blogFolder = new PathUtility().GetBlogFolder(blogName);
            return Path.Combine(blogFolder, GetRssFeedFileName());
        }
    }
}
