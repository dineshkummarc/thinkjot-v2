/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlTrashDP : TrashDP
    {
        public override void EmptyTrash(string blogName)
        {
            try
            {
                string trashFolder = ProviderSettings.GetDeletedPostsFolder(blogName);
                foreach (string filePath in Directory.GetFiles(trashFolder))
                {
                    File.Delete(filePath);
                }
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }
    }
}
