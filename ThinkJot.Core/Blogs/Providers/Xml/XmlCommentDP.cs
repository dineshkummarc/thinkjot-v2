/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    public class XmlCommentDP : CommentDP
    {
        public override List<Comment> GetComments(Guid blogEntryId, string blogName)
        {
            try
            {
                SerializationHelper helper = new SerializationHelper();
                List<Comment> comments = null;
                string file = Utils.GetCommentFilePath(blogEntryId, blogName);
                if (File.Exists(file))
                    comments = (List<Comment>)helper.Deserialize(file, typeof(List<Comment>), ProviderSettings.NamespaceURI);
                return comments != null ? comments : new List<Comment>();
            }
            catch(Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new List<Comment>();
            }
        }

        public override void SaveComment(Comment comment, string blogName)
        {
            try
            {

                Guid blogEntryId = comment.TargetEntryId;
                SerializationHelper helper = new SerializationHelper();
                List<Comment> comments;
                string file = Utils.GetCommentFilePath(blogEntryId, blogName);
                if (File.Exists(file))
                    comments = (List<Comment>)helper.Deserialize(file, typeof(List<Comment>), ProviderSettings.NamespaceURI);
                else
                    comments = new List<Comment>();
                comments.Add(comment);
                helper.Serialize(comments, file, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }

        public override void DeleteComment(Guid blogEntryId, Guid commentGuid, string blogName)
        {
            try
            {

                string file = Utils.GetCommentFilePath(blogEntryId, blogName);
                SerializationHelper helper = new SerializationHelper();
                List<Comment> comments = (List<Comment>)helper.Deserialize(file, typeof(List<Comment>), ProviderSettings.NamespaceURI);
                comments.RemoveAll(delegate(Comment comment)
                {
                    return comment.UniqueId == commentGuid;
                });
                helper.Serialize(comments, file, ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
            }
        }
    }
}
