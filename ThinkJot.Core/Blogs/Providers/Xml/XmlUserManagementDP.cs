using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs.Providers.Xml
{
    class XmlUserManagementDP : UserManagementDP
    {
        public override void SaveUsers(Users users, string blogName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override Users GetUsers(string blogName)
        {
            try
            {
                // Test to see if datarepeater is binding
                //Users users = new Users();
                //User user = new User();
                //user.EMail="Test@Test.com";
                //user.UserName="admin";
                //user.Password="secret";
                //users.Entries.Add(user);
                //string usersPath = Path.Combine(ProviderSettings.GetUsersConfigFolder(), "users2.xml");
                //new SerializationHelper().Serialize(users, usersPath, ProviderSettings.NamespaceURI);
                //return(users);
                
                string usersPath = Path.Combine(ProviderSettings.GetUsersConfigFolder(), "users.xml");
                return (Users)new SerializationHelper().Deserialize(usersPath, typeof(Users), ProviderSettings.NamespaceURI);
            }
            catch (Exception ex)
            {
                new XmlLogger().LogException(ex, blogName);
                return new Users();
            }
        }
    }
}
