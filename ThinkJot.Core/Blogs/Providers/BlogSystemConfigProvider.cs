/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using ThinkJot.Core.Blogs.Providers.Xml;

using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class BlogSystemConfigProvider
    {
        public const string XmlProvider = "Xml";
        public const string SqlProvider = "SqlServer";

        public abstract BlogConfig GetBlogConfig(string blogName);
        public abstract Dictionary<string, BlogConfig> GetAll();
        public abstract void ReloadBlogConfig(string blogName);

        static BlogSystemConfigProvider configProvider;
        public static BlogSystemConfigProvider ConfigProvider
        {
            get { return BlogSystemConfigProvider.configProvider; }
        }

        static BlogSystemConfigProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
            {
                configProvider = new XmlBlogSystemConfigProvider();
                return;
            }
            throw new NotImplementedException("Blog config provider is not implemented.");
        }
    }
}
