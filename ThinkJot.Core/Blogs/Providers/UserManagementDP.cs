using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class UserManagementDP
    {
        public abstract void SaveUsers(Users links, string blogName);
        public abstract Users GetUsers(string blogName);
    }
}