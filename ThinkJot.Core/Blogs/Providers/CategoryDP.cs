/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class CategoryDP
    {
        public abstract void AddCategory(Category category, string blogName);
        public abstract void DeleteCategory(string categoryName, string blogName);
        public abstract Category GetCategory(int id, string blogName);
        public abstract List<Category> GetCategories(List<int> ids, string blogName);
        public abstract List<Category> GetAllCategories(string blogName);
    }
}
