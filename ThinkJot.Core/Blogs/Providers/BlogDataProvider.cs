/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Blogs.Providers.Xml;
using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class BlogDataProvider
    {
        public static BlogEntryDP GetBlogEntryProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlBlogEntryDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static CommentDP GetCommentProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlCommentDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static WebLinksDP GetWebLinksProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlWebLinksDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static CategoryDP GetCategoryProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlCategoryDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static BlogRollDP GetBlogRollProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlBlogRollDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static MonthViewDP GetMonthViewProvider()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlMonthViewDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static FeedsDP GetFeedsDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlFeedsDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static TrashDP GetTrashDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlTrashDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static TemplateMapDP GetTemplateMapDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlTemplateMapDP();
            throw new Exception("The requested data provider is not implemented.");
        }

        public static Logger GetLogger()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlLogger();
            throw new Exception("The requested data provider is not implemented");
        }

        public static PingServicesListDP GetPingServicesListDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlPingServicesListDP();
            throw new Exception("The requested data provider is not implemented");
        }

        public static RawTextEditorDP GetRawTextEditorDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlRawTextEditorDP();
            throw new Exception("The requested data provider is not implemented");
        }

        public static UserManagementDP GetUserManagementDP()
        {
            if (SystemConfiguration.GetConfig().Base.BlogSystem.Provider == DataProviders.Xml)
                return new XmlUserManagementDP();
            throw new Exception("The requested data provider is not implemented");
        }
    }
}