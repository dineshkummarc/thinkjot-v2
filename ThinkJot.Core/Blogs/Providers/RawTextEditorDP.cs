/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class RawTextEditorDP
    {
        public abstract string GetRawSettingsText(string settingName, string blogName);
        public abstract void PersistRawSettingsText(string settingName, string newText, string blogName);
        public abstract string GetRawBlogEntryText(Guid entryId, string blogName);
        public abstract void PersistRawBlogEntryText(Guid entryId, string newText, string blogName);
        public abstract string GetRawBlogCommentsText(Guid entryId, string blogName);
        public abstract void PersistRawBlogCommentsText(Guid entryId, string newText, string blogName);        
    }
}
