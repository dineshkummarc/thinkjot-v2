/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class TemplateMapDP
    {
        public abstract void RemoveEntry(Guid entryId, string blogName);
        public abstract void AddEntry(Guid entryId, string template, string blogName);
        public abstract void RemoveTemplate(string template, string blogName);
        public abstract string GetTemplateForEntry(Guid entryId, string blogName);
    }
}
