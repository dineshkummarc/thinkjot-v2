using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public class DataTag
    {
        string key;
        public string Key
        {
            get { return key; }
            set { key = value; }
        }
        string val;

        public string Value
        {
            get { return val; }
            set { val = value; }
        }
    }
}
