/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs.Providers
{
    public abstract class Logger
    {
        public abstract void WriteToLog(Guid eventId, string what, string blogName);
        public abstract void LogException(Exception ex, string blogName);
        public abstract void WriteToSystemLog(Guid eventId, string what);
        public abstract void LogSystemException(Exception ex);
        public abstract void WriteToActivityLog(Guid eventId, string what, string blogName);
        
        public abstract string ReadLog(DateTime date, string blogName);
        public abstract string ReadSystemLog(DateTime date);
        public abstract string ReadActivityLog(DateTime date, string blogName);        
    }
}
