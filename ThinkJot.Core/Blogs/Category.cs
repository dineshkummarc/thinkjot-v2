/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class Category
    {
        int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public static List<Category> GetCategories(List<int> categoryIds, string blogName)
        {
            CategoryDP provider = BlogDataProvider.GetCategoryProvider();
            return provider.GetCategories(categoryIds, blogName);
        }

        public static List<Category> GetAllCategories(string blogName)
        {
            CategoryDP provider = BlogDataProvider.GetCategoryProvider();
            return provider.GetAllCategories(blogName);
        }

        public static void AddCategory(Category category, string blogName)
        {
            CategoryDP provider = BlogDataProvider.GetCategoryProvider();
            provider.AddCategory(category, blogName);
        }

        public static List<int> GetCategoryIds(string[] categoryNames, string blogName)
        {
            return GetCategoryIds(new List<string>(categoryNames), blogName);
        }

        public static List<int> GetCategoryIds(List<string> categoryNames, string blogName)
        {
            List<int> results = new List<int>();
            List<Category> cats = GetAllCategories(blogName);
            foreach (Category cat in cats)
            {
                if (categoryNames.Contains(cat.Name)) results.Add(cat.Id);
            }
            return results;
        }

        public static List<string> GetCategoryStringsForIds(List<int> ids, string blogName)
        {
            List<string> results = new List<string>();
            List<Category> cats = GetAllCategories(blogName);
            foreach (Category cat in cats)
            {
                if (ids.Contains(cat.Id)) results.Add(cat.Name);
            }
            return results;
        }
    }
}
