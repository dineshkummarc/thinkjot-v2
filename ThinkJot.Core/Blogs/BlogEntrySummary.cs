/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs
{
    public class BlogEntrySummary
    {
        DateTime time;
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        Guid uniqueId;
        public Guid UniqueId
        {
            get { return uniqueId; }
            set { uniqueId = value; }
        }

        string title = "";
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        string author = "";
        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        bool publish;
        public bool Publish
        {
            get { return publish; }
            set { publish = value; }
        }

        public BlogEntrySummary()
        {
        }

        public BlogEntrySummary(DateTime time, Guid uniqueId, string title, string author, bool publish)
        {
            this.time = time;
            this.uniqueId = uniqueId;
            this.title = title;
            this.author = author;
            this.publish = publish;
        }
    }
}
