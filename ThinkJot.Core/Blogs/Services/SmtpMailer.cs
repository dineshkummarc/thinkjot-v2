/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Threading;

using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs.Services
{
    public static class SmtpMailer
    {
        //untested code..
        public static void SendMail(string from, string to, string subject, string body, string blogName)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(delegate(object obj)
                {
                    BlogConfig blogConfig = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
                    string smtpServerName = blogConfig.SmtpServerName;

                    MailMessage message = new MailMessage(from, to, subject, body);
                    try
                    {
                        SmtpClient smtpClient = new SmtpClient(smtpServerName);
                        smtpClient.Port = blogConfig.SmtpPort;

                        if (blogConfig.SmtpAuthenticationRequired)
                            smtpClient.Credentials = new NetworkCredential(blogConfig.SmtpUserName, blogConfig.SmtpPassword);

                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        Logger logger = BlogDataProvider.GetLogger();
                        logger.LogSystemException(ex);
                    }
                }, null);
            }
            catch(Exception ex)
            {
                Logger logger = BlogDataProvider.GetLogger();
                logger.LogSystemException(ex);
            }
        }        
    }
}
