/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CookComputing.XmlRpc;
using ThinkJot.Core.Blogs.Services.MetaWeblog;

namespace ThinkJot.Core.Blogs.Services.MetaWeblog
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public struct Enclosure
    {
        public int length;
        public string type;
        public string url;
    }


    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public struct Source
    {
        public string name;
        public string url;
    }

    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public struct Post
    {
        [XmlRpcMissingMapping(MappingAction.Error)]
        [XmlRpcMember(Description = "Required when posting.")]
        public DateTime dateCreated;

        [XmlRpcMissingMapping(MappingAction.Error)]
        [XmlRpcMember(Description = "Required when posting.")]
        public string description;

        [XmlRpcMissingMapping(MappingAction.Error)]
        [XmlRpcMember(Description = "Required when posting.")]
        public string title;
        [XmlRpcMember]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string[] categories;

        [XmlRpcMember]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string link;

        [XmlRpcMember]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string permalink;
        [XmlRpcMember(
          Description = "Not required when posting. Depending on server may "
          + "be either string or integer. "
          + "Use Convert.ToInt32(postid) to treat as integer or "
          + "Convert.ToString(postid) to treat as string")]
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string postid;
    }

    public struct CategoryInfo
    {
        public string description;
        public string htmlUrl;
        public string rssUrl;
        public string title;
        public string categoryid;
    }


    public struct Category
    {
        public string categoryId;
        public string categoryName;
    }

    public struct MediaObject
    {
        public string name;
        public string type;
        public byte[] bits;
    }

    public struct MediaObjectUrl
    {
        public string url;
    }

    public struct BlogInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string blogid;
        /// <summary>
        /// 
        /// </summary>
        public string url;
        /// <summary>
        /// 
        /// </summary>
        public string blogName;
    }


    public interface IMetaWeblogAPI
    {
        [XmlRpcMethod("metaWeblog.editPost",
             Description = "Updates an existing post to a designated blog "
             + "using the metaWeblog API. Returns true if completed.")]
        bool metaweblog_editPost(
            string postid,
            string username,
            string password,
            MetaWeblog.Post post,
            bool publish);

        [XmlRpcMethod("metaWeblog.getCategories",
             Description = "Retrieves a list of valid categories for a post "
             + "using the metaWeblog API. Returns the metaWeblog categories "
             + "struct collection.")]
        CategoryInfo[] metaweblog_getCategories(
            string blogid,
            string username,
            string password);

        [XmlRpcMethod("metaWeblog.getPost",
             Description = "Retrieves an existing post using the metaWeblog "
             + "API. Returns the metaWeblog struct.")]
        MetaWeblog.Post metaweblog_getPost(
            string postid,
            string username,
            string password);

        [XmlRpcMethod("metaWeblog.getRecentPosts",
             Description = "Retrieves a list of the most recent existing post "
             + "using the metaWeblog API. Returns the metaWeblog struct collection.")]
        MetaWeblog.Post[] metaweblog_getRecentPosts(
            string blogid,
            string username,
            string password,
            int numberOfPosts);

        [XmlRpcMethod("metaWeblog.newPost",
             Description = "Makes a new post to a designated blog using the "
             + "metaWeblog API. Returns postid as a string.")]
        string metaweblog_newPost(
            string blogid,
            string username,
            string password,
            MetaWeblog.Post post,
            bool publish);

        [XmlRpcMethod("metaWeblog.newMediaObject",
             Description = "Upload a new file to the binary content. Returns url as a string")]
        MediaObjectUrl metaweblog_newMediaObject(string blogid, string username, string password, MediaObject enc);

        [XmlRpcMethod("blogger.getUsersBlogs",
        Description = "Returns information on all the blogs a given user "
        + "is a member.")]
        BlogInfo[] blogger_getUsersBlogs(
            string appKey,
            string username,
            string password);
    }
}