/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public static class ActivityLog
    {
        public static ActivityStats GetActivityStats(DateTime which, string blogName)
        {
            List<ActivityLogEntry> entries = new List<ActivityLogEntry>();
            ActivityLogEntry currentEntry = new ActivityLogEntry();
            
            Logger logger = BlogDataProvider.GetLogger();
            string rawLog = logger.ReadActivityLog(which, blogName);
            if (rawLog == null)
                return new ActivityStats(entries);

            StringReader reader = new StringReader(rawLog);
            string aLine;
            while (true)
            {
                aLine = reader.ReadLine();
                if (aLine != null)
                {
                    try
                    {
                        //eg: ThinkJotLog_Event:ed59716e-9f02-4a5d-913f-81503eabf057; ThinkJotLog_Time:3/8/2007 9:48:54 AM
                        if (aLine.StartsWith("ThinkJotLog_Event:"))
                        {
                            currentEntry = new ActivityLogEntry();
                            entries.Add(currentEntry);
                            currentEntry.EventId = new Guid(aLine.Substring(18, 36));
                            currentEntry.Time = DateTime.Parse(aLine.Substring(73));
                        }
                        else
                        {
                            string[] parts = aLine.Split('\t');
                            currentEntry.RawUrl = parts[0];
                            currentEntry.RequestUrl = parts[1];
                            currentEntry.Referrer = parts[2];
                            currentEntry.UserHostAddress = parts[3];
                            currentEntry.UserHostName = parts[4];
                            currentEntry.UserAgent = parts[5];
                        }
                    }
                    catch
                    {
                        //.....
                    }
                }
                else
                    break;
            }

            ActivityStats stats = new ActivityStats(entries);
            return stats;
        }

        public static void WriteToLog(ActivityLogEntry logEntry, string blogName)
        {
            logEntry.RawUrl = logEntry.RawUrl != null ?
                HtmlUtility.GetSafeString(logEntry.RawUrl, 256) : "";
            logEntry.RequestUrl = logEntry.RequestUrl != null ? 
                HtmlUtility.GetSafeString(logEntry.RequestUrl, 256) : "";
            logEntry.Referrer = logEntry.Referrer != null ? 
                HtmlUtility.GetSafeString(logEntry.Referrer, 256) : "";
            logEntry.UserHostAddress = logEntry.UserHostAddress != null ? 
                HtmlUtility.GetSafeString(logEntry.UserHostAddress, 20) : ""; //ip address
            logEntry.UserHostName = logEntry.UserHostName != null ? 
                HtmlUtility.GetSafeString(logEntry.UserHostName, 256) : "";
            logEntry.UserAgent = logEntry.UserAgent != null ? 
                HtmlUtility.GetSafeString(logEntry.UserAgent, 256) : "";

            Guid eventId = Guid.NewGuid();
            string requestDetails = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}",
                logEntry.RawUrl,
                logEntry.RequestUrl,
                logEntry.Referrer,
                logEntry.UserHostAddress,
                logEntry.UserHostName,
                logEntry.UserAgent);

            Logger logger = BlogDataProvider.GetLogger();
            logger.WriteToActivityLog(eventId, requestDetails, blogName);          
        }
    }
}
