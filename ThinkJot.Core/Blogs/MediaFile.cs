/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class MediaFile
    {
        string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        DateTime fileDate;
        public DateTime FileDate
        {
            get { return fileDate; }
            set { fileDate = value; }
        }

        public static List<MediaFile> GetMediaFiles(string blogName)
        {
            try
            {
                List<MediaFile> results = new List<MediaFile>();
                MediaFile mediaFile;
                string mediaFolder = new PathUtility().GetBlogMediaFolder(blogName);
                string[] files = Directory.GetFiles(mediaFolder);
                foreach (string file in files)
                {
                    mediaFile = new MediaFile();
                    FileInfo fileInfo = new FileInfo(file);
                    mediaFile.FileDate = fileInfo.CreationTime;
                    mediaFile.FileName = Path.GetFileName(fileInfo.FullName);
                    results.Add(mediaFile);
                }
                return results;
            }
            catch (Exception ex)
            {
                Logger logger = BlogDataProvider.GetLogger();
                logger.LogException(ex, blogName);
                return new List<MediaFile>();
            }
        }

        public static void DeleteFile(string fileName, string blogName)
        {
            try
            {
                string filePath = Path.Combine(new PathUtility().GetBlogMediaFolder(blogName), fileName);
                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                Logger logger = BlogDataProvider.GetLogger();
                logger.LogException(ex, blogName);
            }
        }
    }
}
