/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Blogs
{
    public class TemplateMapEntry
    {
        Guid entryId;
        public Guid EntryId
        {
            get { return entryId; }
            set { entryId = value; }
        }

        string template;
        public string Template
        {
            get { return template; }
            set { template = value; }
        }
    }
    public class TemplateMap
    {
        List<TemplateMapEntry> entries = new List<TemplateMapEntry>();
        public List<TemplateMapEntry> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public static void RemoveEntry(Guid entryId, string blogName)
        {
            TemplateMapDP dataProvider = BlogDataProvider.GetTemplateMapDP();
            dataProvider.RemoveEntry(entryId, blogName);
        }

        public static void AddEntry(Guid entryId, string template, string blogName)
        {
            TemplateMapDP dataProvider = BlogDataProvider.GetTemplateMapDP();
            dataProvider.AddEntry(entryId, template, blogName);
        }

        public static void RemoveTemplate(string template, string blogName)
        {
            TemplateMapDP dataProvider = BlogDataProvider.GetTemplateMapDP();
            dataProvider.RemoveTemplate(template, blogName);            
        }

        public static string GetTemplateForEntry(Guid entryId, string blogName)
        {
            BlogConfig config = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
            if (config.UseTemplateMap)
            {
                TemplateMapDP dataProvider = BlogDataProvider.GetTemplateMapDP();
                string template = dataProvider.GetTemplateForEntry(entryId, blogName);
                if (template == null) return "default.aspx";
                else return template;
            }
            else
                return "default.aspx";
        }
    }
}
