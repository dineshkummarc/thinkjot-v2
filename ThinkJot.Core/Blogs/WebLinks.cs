/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class WebLinks
    {
        List<WebLink> entries = new List<WebLink>();
        public List<WebLink> Entries
        {
            get { return entries; }
            set { entries = value; }
        }
       
        public static WebLinks Load(string blogName)
        {
            WebLinksDP provider = BlogDataProvider.GetWebLinksProvider();
            return provider.GetWebLinks(blogName);
        }

        public void Save(string blogName)
        {
            WebLinksDP provider = BlogDataProvider.GetWebLinksProvider();
            provider.SaveWebLinks(this, blogName);
        }
    }
}
