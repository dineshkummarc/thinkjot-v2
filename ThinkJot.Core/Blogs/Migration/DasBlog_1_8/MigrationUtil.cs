/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using ThinkJot.Core.Blogs;

namespace ThinkJot.Core.Blogs.Migration.DasBlog_1_8
{
    public class MigrationUtil : IMigrationUtil
    {
        public void Run(string folderName, string blogName)
        {
            if (!Directory.Exists(folderName)) return;

            string[] files = Directory.GetFiles(folderName, "*.dayentry.xml");
            foreach (string file in files)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(file);
                ConvertEntries(doc, blogName);
            }

            files = Directory.GetFiles(folderName, "*.dayfeedback.xml");
            foreach (string file in files)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(file);
                ConvertComments(doc, blogName);
            }
        }

        private void ConvertEntries(XmlDocument doc, string blogName)
        {
            XmlNamespaceManager nsman = new XmlNamespaceManager(doc.NameTable);
            nsman.AddNamespace("das", "urn:newtelligence-com:dasblog:runtime:data");

            XmlNodeList entries = doc.SelectNodes("//das:Entry", nsman);
            foreach (XmlNode entry in entries)
            {
                List<Category> cats = Category.GetAllCategories(blogName);
                int catCount = cats.Count;

                BlogEntry post = new BlogEntry();

                XmlNode node = entry.SelectSingleNode("das:Created", nsman);
                if (node != null) post.Time = System.Convert.ToDateTime(node.InnerText);

                node = entry.SelectSingleNode("das:Modified", nsman);
                if (node != null) post.ModifiedTime = System.Convert.ToDateTime(node.InnerText);

                node = entry.SelectSingleNode("das:EntryId", nsman);
                if (node != null) post.UniqueId = new Guid(node.InnerText);

                node = entry.SelectSingleNode("das:Description", nsman);
                if (node != null) post.Description = node.Value;

                node = entry.SelectSingleNode("das:Title", nsman);
                if (node != null) post.Title = node.InnerText;

                node = entry.SelectSingleNode("das:Categories", nsman);
                if (node != null)
                {
                    string strCats = node.InnerText;
                    string[] arrCats = strCats.Split(';');
                    foreach (string strCat in arrCats)
                    {
                        if (string.IsNullOrEmpty(strCat)) continue;
                        bool found = false;
                        foreach (Category cat in cats)
                        {
                            if (cat.Name == strCat)
                            {
                                post.CategoryIds.Add(cat.Id);
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            Category newCat = new Category();
                            newCat.Id = catCount;
                            newCat.Name = strCat;
                            newCat.Description = strCat;
                            Category.AddCategory(newCat, blogName);                            
                            catCount++;
                        }
                    }
                }                

                node = entry.SelectSingleNode("das:Author", nsman);
                if (node != null) post.Author = node.InnerText;

                node = entry.SelectSingleNode("das:Syndicated", nsman);
                if (node != null) post.Syndicate = System.Convert.ToBoolean(node.InnerText);

                node = entry.SelectSingleNode("das:AllowComments", nsman);
                if (node != null) post.AllowComments = System.Convert.ToBoolean(node.InnerText);

                node = entry.SelectSingleNode("das:Content", nsman);
                if (node != null)
                {
                    post.Content = node.InnerText;
                    post.Content = post.Content.Replace("<br>", "<br />");
                }

                post.Publish = true;

                post.Save(blogName);
            }
        }

        private void ConvertComments(XmlDocument doc, string blogName)
        {
            XmlNamespaceManager nsman = new XmlNamespaceManager(doc.NameTable);
            nsman.AddNamespace("das", "urn:newtelligence-com:dasblog:runtime:data");

            XmlNodeList entries = doc.SelectNodes("//das:Comment", nsman);
            foreach (XmlNode entry in entries)
            {
                Comment comment = new Comment();

                XmlNode node = entry.SelectSingleNode("das:Created", nsman);
                if (node != null) comment.Time = System.Convert.ToDateTime(node.InnerText);

                node = entry.SelectSingleNode("das:Modified", nsman);
                if (node != null) comment.ModifiedTime = System.Convert.ToDateTime(node.InnerText);

                node = entry.SelectSingleNode("das:EntryId", nsman);
                if (node != null) comment.UniqueId = new Guid(node.InnerText);

                node = entry.SelectSingleNode("das:TargetEntryId", nsman);
                if (node != null) comment.TargetEntryId = new Guid(node.InnerText);

                node = entry.SelectSingleNode("das:Author", nsman);
                if (node != null) comment.Author = node.InnerText;

                node = entry.SelectSingleNode("das:AuthorEmail", nsman);
                if (node != null) comment.AuthorEmail = node.InnerText;

                node = entry.SelectSingleNode("das:AuthorHomepage", nsman);
                if (node != null) comment.AuthorHomepage = node.InnerText;

                node = entry.SelectSingleNode("das:AuthorIPAddress", nsman);
                if (node != null) comment.AuthorIpAddress = node.InnerText;

                Comment.SaveComment(comment, blogName);
            }
        }
    }
}
