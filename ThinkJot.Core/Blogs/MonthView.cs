/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    public class MonthViewEntry
    {
        private int month;
        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        private int year;
        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        private int numPosts;
        public int NumPosts
        {
            get { return numPosts; }
            set { numPosts = value; }
        }

        public MonthViewEntry(int month, int year, int numPosts)
        {
            this.month = month;
            this.year = year;
            this.numPosts = numPosts;
        }

        public string DisplayString
        {
            get
            {
                return GetMonthName(month) + ", " + year.ToString() + " (" + numPosts.ToString() + ")";
            }
        }

        private string GetMonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }
            return null;
        }
    }

    public class MonthView : Collection<string>
    {
        List<MonthViewEntry> entries = new List<MonthViewEntry>();
        public List<MonthViewEntry> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public static MonthView GetMonthView(string blogName)
        {
            MonthViewDP dataProvider = BlogDataProvider.GetMonthViewProvider();
            return dataProvider.GetMonthView(blogName);
        }
    }
}
