/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Web;

using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Blogs.Services;

namespace ThinkJot.Core.Blogs
{
    //Comments are risky. Encode everything.
    [Serializable]
    public class Comment
    {        
        public Comment()
        {
            uniqueId = Guid.NewGuid();
            time = DateTime.Now.ToUniversalTime();
        }

        private long id;
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        private Guid uniqueId;
        public Guid UniqueId
        {
            get { return uniqueId; }
            set { uniqueId = value; }
        }

        private string author;
        public string Author
        {
            get 
            {
                return author; 
            }
            set 
            {
                author = value; 
            }
        }

        private string authorEmail;
        public string AuthorEmail
        {
            get 
            {
                return authorEmail;
            }
            set { authorEmail = value; }
        }

        private string authorHomepage;
        public string AuthorHomepage
        {
            get 
            {
                return authorHomepage; 
            }
            set 
            { 
                authorHomepage = value; }
        }

        private string authorIpAddress;
        public string AuthorIpAddress
        {
            get
            {
                return authorIpAddress; 
            }
            set 
            { 
                authorIpAddress = value;
            }
        }

        private Guid targetEntryId;
        public Guid TargetEntryId
        {
            get { return targetEntryId; }
            set { targetEntryId = value; }
        }

        private DateTime time;
        public DateTime Time
        {
            get { return time; }
            set 
            {
                time = value.ToUniversalTime();
            }
        }

        private DateTime modifiedTime;
        public DateTime ModifiedTime
        {
            get { return modifiedTime; }
            set { modifiedTime = value.ToUniversalTime(); }
        }

        private string text;
        public string Text
        {
            get 
            {
                return text; 
            }
            set 
            { 
                text = value;
            }
        }

        private string externalLink;
        public string ExternalLink
        {
            get 
            {
                return externalLink; 
            }
            set 
            {
                externalLink = value;
            }
        }

        public static List<Comment> GetComments(Guid blogEntryId, string blogName)
        {
            CommentDP dataProvider = BlogDataProvider.GetCommentProvider();
            return dataProvider.GetComments(blogEntryId, blogName);
        }

        public static void SaveComment(Comment comment, string blogName)
        {
            CommentDP dataProvider = BlogDataProvider.GetCommentProvider();
            dataProvider.SaveComment(comment, blogName);

            BlogConfig blogConfig = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
            if (blogConfig.EmailAuthorOnComment)
            {
                BlogEntry blogEntry = BlogEntry.GetBlogEntry(comment.TargetEntryId, blogName);
                string author = blogEntry.Author;
                string authorEmail = System.Web.Security.Membership.GetUser(author).Email;
                string subject = "New comment for " + blogEntry.Title;
                string body = comment.Author + " (" + comment.authorEmail + ") " + 
                    "has written a new comment for your post titled " + blogEntry.Title + ".";
                body += "\n";
                body += "To disable automatic comment notification, edit your blog configuration.";
                SmtpMailer.SendMail(authorEmail, authorEmail, subject, body, blogName);
            }            
        }

        public static void DeleteComment(Guid blogEntryId, Guid commentGuid, string blogName)
        {
            CommentDP dataProvider = BlogDataProvider.GetCommentProvider();
            dataProvider.DeleteComment(blogEntryId, commentGuid, blogName);
        }        
    }

}
