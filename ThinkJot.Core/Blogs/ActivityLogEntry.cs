/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs
{
    public class ActivityLogEntry : LogEntry
    {
        string rawUrl;
        public string RawUrl
        {
            get { return rawUrl; }
            set { rawUrl = value; }
        }


        string requestUrl;
        public string RequestUrl
        {
            get { return requestUrl; }
            set { requestUrl = value; }
        }


        string referrer;
        public string Referrer
        {
            get { return referrer; }
            set { referrer = value; }
        }


        string userHostAddress;
        public string UserHostAddress
        {
            get { return userHostAddress; }
            set { userHostAddress = value; }
        }


        string userHostName;
        public string UserHostName
        {
            get { return userHostName; }
            set { userHostName = value; }
        }


        string userAgent;
        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }
    }
}
