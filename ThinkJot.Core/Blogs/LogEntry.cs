using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs
{
    public class LogEntry
    {
        Guid eventId;
        public Guid EventId
        {
            get { return eventId; }
            set { eventId = value; }
        }

        DateTime time;
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
