/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Blogs
{
    public class PingServicesList
    {
        List<string> entries = new List<string>();
        public List<string> Entries
        {
            get { return entries; }
            set { entries = value; }
        }
    }

    public class UpdatePingInfo
    {
        string pingServiceName;
        public string PingServiceName
        {
            get { return pingServiceName; }
            set { pingServiceName = value; }
        }

        string url;
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        string endPoint;
        public string EndPoint
        {
            get { return endPoint; }
            set { endPoint = value; }
        }

        PingAPIType apiType;
        public PingAPIType APIType
        {
            get { return apiType; }
            set { apiType = value; }
        }

        private static UpdatePingInfo GetUpdatePingInfo(string serviceName)
        {
            UpdatePingInfo pingInfo = null;
            switch (serviceName)
            {
                case "blo.gs":
                    pingInfo = new UpdatePingInfo();
                    pingInfo.PingServiceName = "blo.gs";
                    pingInfo.Url = "http://www.blo.gs";
                    pingInfo.EndPoint = "http://ping.blo.gs/";
                    pingInfo.APIType = PingAPIType.Extented;
                    break;
                case "weblogs":
                    pingInfo = new UpdatePingInfo();
                    pingInfo.PingServiceName = "weblogs";
                    pingInfo.Url = "http://www.weblogs.com";
                    pingInfo.EndPoint = "http://rpc.weblogs.com/RPC2";
                    pingInfo.APIType = PingAPIType.Basic;
                    break;
                case "technorati":
                    pingInfo = new UpdatePingInfo();
                    pingInfo.PingServiceName = "technorati";
                    pingInfo.Url = "http://rpc.technorati.com";
                    pingInfo.EndPoint = "http://rpc.technorati.com/rpc/ping";
                    pingInfo.APIType = PingAPIType.Basic;
                    break;
                case "google-blogsearch":
                    pingInfo = new UpdatePingInfo();
                    pingInfo.PingServiceName = "google-blogsearch";
                    pingInfo.Url = "http://blogsearch.google.com";
                    pingInfo.EndPoint = "http://blogsearch.google.com/ping/RPC2";
                    pingInfo.APIType = PingAPIType.Extented;
                    break;
            }
            return pingInfo;
        }

        public static void PingAll(string blogName)
        {
            try
            {
                PingServicesListDP svcListDP = BlogDataProvider.GetPingServicesListDP();
                PingServicesList svcList = svcListDP.GetPingServicesList(blogName);

                List<UpdatePingInfo> pingInfos = new List<UpdatePingInfo>();
                foreach (string svc in svcList.Entries)
                {
                    pingInfos.Add(GetUpdatePingInfo(svc));
                }

                PathUtility pathUtil = new PathUtility();

                foreach (UpdatePingInfo pingInfo in pingInfos)
                {
                    try
                    {
                        if (pingInfo.APIType == PingAPIType.Basic)
                        {
                            WeblogUpdatesClientProxy updates = new WeblogUpdatesClientProxy(pingInfo.EndPoint);
                            WeblogUpdatesReply reply = updates.Ping(blogName, pathUtil.GetMappedBlogUrl(blogName));
                            if (reply.flerror)
                            {
                                Logger logger = BlogDataProvider.GetLogger();
                                logger.WriteToLog(Guid.NewGuid(), String.Format("Notifying {0}: {1}", pingInfo.PingServiceName, reply.message), blogName);
                            }
                        }
                        else if (pingInfo.APIType == PingAPIType.Extented)
                        {
                            ExtendedWeblogUpdatesClientProxy updates = new ExtendedWeblogUpdatesClientProxy(pingInfo.EndPoint);
                            string rssFeed = pathUtil.CombineUrls(pathUtil.GetBlogUrl(blogName), "rss.xml");
                            WeblogUpdatesReply reply = updates.ExtendedPing(blogName, pathUtil.GetMappedBlogUrl(blogName), "", rssFeed);
                            if (reply.flerror)
                            {
                                Logger logger = BlogDataProvider.GetLogger();
                                logger.WriteToLog(Guid.NewGuid(), String.Format("Notifying {0}: {1}", pingInfo.PingServiceName, reply.message), blogName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger logger = BlogDataProvider.GetLogger();
                        logger.LogException(ex, blogName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger logger = BlogDataProvider.GetLogger();
                logger.LogException(ex, blogName);
            }
        }
    }

    public enum PingAPIType
    {
        Basic = 1,
        Extented = 2 // for blo.gs
    }
}