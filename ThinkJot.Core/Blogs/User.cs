/*************************************************
 * ThinkJot V2
 * Author: Charlie 
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs
{
    [Serializable]
    public class User
    {
        string  username;
        public string UserName
        {
            get { return username; }
            set { username = value; }
        }
        string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        string email;
        public string EMail
        {
            get { return email; }
            set { email = value; }
        }
        
        public User()
        {
            //..
        }

        public User(string username, string password, string email)
        {
            this.username = username;
            this.password = password;
            this.email = email;
        }
    }
}
