/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ThinkJot.Core.Blogs
{
    public class BlogRollEntry
    {
        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        string xmlUrl;
        public string XmlUrl
        {
            get { return xmlUrl; }
            set { xmlUrl = value; }
        }

        string htmlUrl;
        public string HtmlUrl
        {
            get { return htmlUrl; }
            set { htmlUrl = value; }
        }

        public BlogRollEntry()
        {
        }

        public BlogRollEntry(XmlNode opmlNode)
        {
            description = GetValueOrEmptyIfNull(opmlNode, "description");
            title = GetValueOrEmptyIfNull(opmlNode, "title");
            xmlUrl = GetValueOrEmptyIfNull(opmlNode, "xmlUrl");
            htmlUrl = GetValueOrEmptyIfNull(opmlNode, "htmlUrl");
        }

        private string GetValueOrEmptyIfNull(XmlNode node, string attributeName)
        {
            if (node.Attributes[attributeName] == null) return "";
            else return node.Attributes[attributeName].Value;
        }
    }
}
