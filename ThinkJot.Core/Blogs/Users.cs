/*************************************************
 * ThinkJot V2
 * Author: Charlie 
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Blogs
{
    [Serializable]

    public class Users
    {
        List<User> entries = new List<User>();
        public List<User> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public static Users Load(string blogName)
        {
            UserManagementDP provider = BlogDataProvider.GetUserManagementDP();
            return provider.GetUsers(blogName);
        }

        public void Save(string blogName)
        {
            UserManagementDP provider = BlogDataProvider.GetUserManagementDP();
            provider.SaveUsers(this, blogName);
        }
    }
}
