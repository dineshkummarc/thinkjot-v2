/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core.Blogs
{
    public class WebLink
    {
        int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        string url;
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public WebLink()
        {
            //..
        }

        public WebLink(int id, string url, string name, string description)
        {
            this.id = id;
            this.url = url;
            this.name = name;
            this.description = description;
        }
    }
}
