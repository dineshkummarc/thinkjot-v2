/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ThinkJot.Core.Utils
{
    public static class StringUtils
    {
        public static string StripWhitespace_And_SpecialChars(string input)
        {

            return Regex.Replace(input, @"[^\w]", "");
            ////TODO: A better impl
            //if (string.IsNullOrEmpty(title)) return title;
            //title = RemoveThese(title, "<", ">", ".", "!", " ");
            //title = StripWhiteSpace(title);
            //return title;
        }

        private static string StripWhiteSpace(string input)
        {
            return input.Replace(" ", "");
        }

        public static string RemoveThese(string input, params string[] removables)
        {
            foreach (string removable in removables)
                input = input.Replace(removable, "");
            return input;
        }
    }
}
