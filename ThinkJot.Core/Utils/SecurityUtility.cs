/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;

using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Utils
{
    public class SecurityUtility
    {
        public bool IsBlogEditor(string blogName)
        {
            MembershipUser user = System.Web.Security.Membership.GetUser();
            if (user == null) return false;
            
            if (IsSystemAdministrator(user)) return true;
            
            BlogConfig config = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
            return (user.UserName == config.Administrator || config.Editors.Contains(user.UserName));
        }

        public bool IsLoggedIn()
        {
            MembershipUser user = System.Web.Security.Membership.GetUser();
            return user != null;
        }

        public bool IsBlogAdministrator(string blogName)
        {
            MembershipUser user = System.Web.Security.Membership.GetUser();
            if (user == null) return false;
            
            if (IsSystemAdministrator(user)) return true;
            return SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName).Administrator == user.UserName;
        }

        public bool IsSystemAdministrator()
        {
            MembershipUser user = System.Web.Security.Membership.GetUser();
            if (user == null) return false;
            return IsSystemAdministrator(user);
        }

        public bool IsSystemAdministrator(MembershipUser user)
        {
            return user.UserName == "administrator";
        }
    }
}