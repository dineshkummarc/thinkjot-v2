/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace ThinkJot.Core.Utils
{
    public static class HtmlUtility
    {
        // Need to remove unwanted chars from js params
        //TODO: Implement
        public static string FormatJavascriptParams(string text)
        {
            return text;
        }

        public static string GetSafeString(string html)
        {
            return GetSafeString(html, 0);
        }

        public static string GetSafeString(string html, int maxLength)
        {
            html.Replace("<", "&lt;").Replace(">", "&gt;");
            if (maxLength > 0)
                if (html.Length > maxLength)
                    return "";                
            return html;
        }
    }
}
