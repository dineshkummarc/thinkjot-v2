/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ThinkJot.Core.Configuration;

namespace ThinkJot.Core.Utils
{
    public class PathUtility
    {
        public string CombineUrls(string part1, string part2)
        {
            if (part2.StartsWith("/")) return part2;
            if (part1.EndsWith("/")) return part1 + part2;
            return part1 + "/" + part2;
        }

        public string GetBlogFolder(string blogName)
        {
            return Path.Combine(SystemConfiguration.GetConfig().BlogsFolder, blogName);
        }


        //Returns the REAL Path, without Url Mapping
        public string GetBlogUrl(string blogName)
        {
            return CombineUrls(SystemConfiguration.GetConfig().BlogsUrl, blogName);
        }

        //Returns Url Mapped Url if url mapping is ON
        public string GetMappedBlogUrl(string blogName)
        {
            BlogConfig blogConfig = SystemConfiguration.GetConfig().Blogs.GetBlogConfig(blogName);
            if (blogConfig.UseUrlMapping)
                return TranslateRelativeUrl(blogConfig.AlternateUrl);
            else return CombineUrls(SystemConfiguration.GetConfig().BlogsUrl, blogName);
        }

        public string GetBlogMediaUrl(string blogName)
        {
            return CombineUrls(GetBlogUrl(blogName), "Media");
        }

        public string GetBlogMediaFolder(string blogName)
        {
            return Path.Combine(GetBlogFolder(blogName), "Media");
        }

        public string TranslateRelativeUrl(string relativeUrl)
        {
            relativeUrl = relativeUrl.TrimStart('/');
            return CombineUrls(SystemConfiguration.GetConfig().SiteRoot, relativeUrl);
        }
    }
}

