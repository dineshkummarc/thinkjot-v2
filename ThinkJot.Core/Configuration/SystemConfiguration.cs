/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Globalization;
using System.IO;

using ThinkJot.Core.Utils;
using ThinkJot.Core.Blogs.Providers;

namespace ThinkJot.Core.Configuration
{
    public class SystemConfiguration
    {
        private SystemConfiguration() { }

        private static SystemConfiguration config;
        public static SystemConfiguration GetConfig()
        {
            return config;
        }

        BlogSystemConfigProvider blogs;
        public BlogSystemConfigProvider Blogs
        {
            get { return blogs; }
        }
        
        ThinkJotBaseConfigurationSection _base;
        public ThinkJotBaseConfigurationSection Base
        {
            get { return _base; }
        }

        public string SiteRoot
        {
            get { return _base.SiteInfo.RootUrl; }
        }

        public string ForumsFolder
        {
            get
            {
                return Path.Combine(appRoot, "Forums");
            }
        }

        public string BlogsFolder
        {
            get
            {
                return Path.Combine(appRoot, "Blogs");
            }
        }

        public string ComponentsUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "_Site/Components");
            }
        }

        public string ScriptsUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "_Site/Scripts");
            }
        }

        public string ImagesUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "_Site/Images");
            }
        }

        public string StyleUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "_Site/Style");
            }
        }

        public string ForumsUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "Forums");
            }
        }

        public string BlogsUrl
        {
            get
            {
                return new PathUtility().CombineUrls(SiteRoot, "Blogs");
            }
        }

        string appRoot;
        public string AppRoot
        {
            get { return appRoot; }
        }

        string userAgent = "ThinkJot V2.0";
        public string UserAgent
        {
            get { return userAgent; }
            set { userAgent = value; }
        }
        

        static SystemConfiguration()
        {
            config = new SystemConfiguration();
            config.appRoot = HttpContext.Current.Request.MapPath("~");
            config._base = (ThinkJotBaseConfigurationSection)System.Web.Configuration.WebConfigurationManager.GetSection("thinkJotBaseConfiguration");
            config.blogs = BlogSystemConfigProvider.ConfigProvider;
        }
    }
}