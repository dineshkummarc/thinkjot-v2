/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using ThinkJot.Core.Utils;

namespace ThinkJot.Core.Configuration
{
    public class BlogConfig
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value.ToLower(); }
        }

        private string blogId;
        public string BlogId
        {
            get 
            {
                if (string.IsNullOrEmpty(blogId))
                    return name;
                return blogId;
            }
            set { blogId = value; }
        }

        private string blogTitle;
        public string BlogTitle
        {
            get { return blogTitle; }
            set { blogTitle = value; }
        }

        private string alternateUrl;
        public string AlternateUrl
        {
            get { return alternateUrl; }
            set { alternateUrl = value; }
        }

        private string administrator;
        public string Administrator
        {
            get { return administrator; }
            set { administrator = value; }
        }

        private List<string> editors = new List<string>();
        public List<string> Editors
        {
            get { return editors; }
            set { editors = value; }
        }

        private int numEntriesOnPage = 10;
        public int NumEntriesOnPage
        {
            get { return numEntriesOnPage; }
            set { numEntriesOnPage = value; }
        }

        //-------- FEEDS -----------
        private int numItemsOnFeed = 10;
        public int NumItemsOnFeed
        {
            get { return numItemsOnFeed; }
            set { numItemsOnFeed = value; }
        }

        private int maxSearchResults = 10;
        public int MaxSearchResults
        {
            get { return maxSearchResults; }
            set { maxSearchResults = value; }
        }

        private bool enableRssHeaderAndFooter = false;
        public bool EnableRssHeaderAndFooter
        {
            get { return enableRssHeaderAndFooter; }
            set { enableRssHeaderAndFooter = value; }
        }

        private string rssHeader;
        public string RssHeader
        {
            get { return rssHeader; }
            set { rssHeader = value; }
        }

        private string rssFooter;
        public string RssFooter
        {
            get { return rssFooter; }
            set { rssFooter = value; }
        }

        private bool truncateContentInFeed = false;
        public bool TruncateContentInFeed
        {
            get { return truncateContentInFeed; }
            set { truncateContentInFeed = value; }            
        }
        //-----------------------------

        private bool showCommentCount = true;
        public bool ShowCommentCount
        {
            get { return showCommentCount; }
            set { showCommentCount = value; }
        }

        private bool useCaptchaForComments = true;
        public bool UseCaptchaForComments
        {
            get { return useCaptchaForComments; }
            set { useCaptchaForComments = value; }
        }

        private bool autoSavePosts = false;
        public bool AutoSavePosts
        {
            get { return autoSavePosts; }
            set { autoSavePosts = value; }
        }

        private bool useTitleForLinks = true;
        public bool UseTitleForLinks
        {
            get { return useTitleForLinks; }
            set { useTitleForLinks = value; }
        }

        private bool useUrlMapping = true;
        public bool UseUrlMapping
        {
            get { return useUrlMapping; }
            set { useUrlMapping = value; }
        }

        //Usually this should contain just 'aspx'
        private List<string> urlMapExtensions = new List<string>();
        public List<string> UrlMapExtensions
        {
            get { return urlMapExtensions; }
            set { urlMapExtensions = value; }
        }

        private string contact;
        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        private string contactEmail;
        public string ContactEmail
        {
            get { return contactEmail; }
            set { contactEmail = value; }
        }

        private string copyright;
        public string Copyright
        {
            get { return copyright; }
            set { copyright = value; }
        }

        private bool enableMetaWeblogAPI = true;
        public bool EnableMetaWeblogAPI
        {
            get { return enableMetaWeblogAPI; }
            set { enableMetaWeblogAPI = value; }
        }

        private bool pingWeblogUpdateTrackers = true;
        public bool PingWeblogUpdateTrackers
        {
            get { return pingWeblogUpdateTrackers; }
            set { pingWeblogUpdateTrackers = value; }
        }

        private bool enableActivityTracking = true;
        public bool EnableActivityTracking
        {
            get { return enableActivityTracking; }
            set { enableActivityTracking = value; }
        }

        private bool emailAuthorOnComment = false;
        public bool EmailAuthorOnComment
        {
            get { return emailAuthorOnComment; }
            set { emailAuthorOnComment = value; }
        }

        private string smtpServerName;
        public string SmtpServerName
        {
            get { return smtpServerName; }
            set { smtpServerName = value; }
        }

        private int smtpPort = 25;
        public int SmtpPort
        {
            get { return smtpPort; }
            set { smtpPort = value; }
        }

        private bool smtpAuthenticationRequired = false;
        public bool SmtpAuthenticationRequired
        {
            get { return smtpAuthenticationRequired; }
            set { smtpAuthenticationRequired = value; }
        }

        private string smtpUserName = "";
        public string SmtpUserName
        {
            get { return smtpUserName; }
            set { smtpUserName = value; }
        }

        private string smtpPassword = "";
        public string SmtpPassword
        {
            get { return smtpPassword; }
            set { smtpPassword = value; }
        }

        //tagging is usually a better idea!
        private bool disableCategories = false;
        public bool DisableCategories
        {
            get { return disableCategories; }
            set { disableCategories = value; }
        }

        private bool useUniversalTime = true;
        public bool UseUniversalTime
        {
            get { return useUniversalTime; }
            set { useUniversalTime = value; }
        }

        //Local time uses the timezone of the system hosting ThinkJot
        //This field should be applicable only if UseUniversalTime = false;
        //default value is set to true; which applies ONLY WHEN UseUniversalTime = false
        private bool useServerLocalTime = true;
        public bool UseServerLocalTime
        {
            get { return useServerLocalTime; }
            set { useServerLocalTime = value; }
        }

        //specify values like "+05:30"; for India.
        //the colon (:) and (+/-) are important
        //This field should be applicable only if UseUniversalTime = false && UseLocalTime = false
        private string utcOffset = "";
        public string UtcOffset
        {
            get { return utcOffset; }
            set 
            {
                if (!value.Contains(":"))
                    throw new Exception("Invalid UTC Offset specified. Offset should contain a colon (:)");
                if (!value.Contains("+") && !value.Contains("-"))
                    throw new Exception("Invalid UTC Offset specified. Offset should contain a plus or minus (+/-)");
                utcOffset = value; 
            }
        }

        //Template Map allows post-specific templates. 
        private bool useTemplateMap = false;
        public bool UseTemplateMap
        {
            get { return useTemplateMap; }
            set { useTemplateMap = value; }
        }
   }
}