/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web.Configuration;

namespace ThinkJot.Core.Configuration
{
    public class ThinkJotBaseConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("blogSystem")]
        public BlogSystemElement BlogSystem
        {
            get
            {
                BlogSystemElement blogSystem =
                (BlogSystemElement)base["blogSystem"];
                return blogSystem;
            }
        }

        [ConfigurationProperty("siteInfo")]
        public SiteInfoElement SiteInfo
        {
            get
            {
                SiteInfoElement siteInfo =
                (SiteInfoElement)base["siteInfo"];
                return siteInfo;
            }
        }
    }

    public class BlogSystemElement : ConfigurationElement
    {
        [ConfigurationProperty("provider",
        IsRequired = true,
        IsKey = true)]
        public string Provider
        {
            get
            {
                return (string)this["provider"];
            }
            set
            {
                this["provider"] = value;
            }
        }
    }

    public class SiteInfoElement : ConfigurationElement
    {
        [ConfigurationProperty("rootUrl",
        IsRequired = true,
        IsKey = true)]
        public string RootUrl
        {
            get
            {
                string rootUrl = (string)this["rootUrl"];
                if (rootUrl.EndsWith("/")) rootUrl = rootUrl.Remove(rootUrl.Length - 1, 1);
                return rootUrl;
            }
            set
            {
                this["rootUrl"] = value;
            }
        }
    }
}
