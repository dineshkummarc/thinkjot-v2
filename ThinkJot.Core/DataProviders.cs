/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * Website: http://www.process64.com/thinkjot/
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ThinkJot.Core
{
    public class DataProviders
    {
        public const string Xml = "Xml";
        public const string SqlServer = "SqlServer";
        public const string SqlServer2005 = "SqlServer2005";
        public const string PostgreSql = "PostgreSql";
        public const string Oracle = "Oracle";
        public const string MySql = "MySql";
    }
}