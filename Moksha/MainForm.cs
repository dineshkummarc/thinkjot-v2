/*************************************************
 * ThinkJot V2
 * Author: Jeswin P. (jeswin@process64.com)
 * 
 * This code is distributed under the terms of the
 * Apache License, Version 2.0.
 * **********************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ThinkJot.Core;
using ThinkJot.Core.Utils;
using ThinkJot.Core.Configuration;
using ThinkJot.Core.Blogs;
using ThinkJot.Core.Blogs.Providers.Xml;

namespace Moksha
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //private void createBoomButton_Click(object sender, EventArgs e)
        //{
        //    BlogConfig config = new BlogConfig();
        //    config.BlogTitle = titleTextBox.Text;
        //    config.Name = "Process64";
        //    config.UrlMapExtensions = new List<string>();
        //    config.UrlMapExtensions.Add("aspx");
        //    new SerializationHelper().Serialize(config, "boomshanker.txt");

        //    //WebLink link = new WebLink();
        //    //link.Description = "Conviction of Life";
        //    //link.Id = 1;
        //    //link.Name = "Jeswin's Blog";
        //    //link.Url = "http://www.process64.com/jeswin";
        //    //WebLinks links = new WebLinks();
        //    //links.Entries.Add(link);
        //    //new SerializationHelper().Serialize(links, "boomshanker.txt");

        //    //BlogEntry entry = new BlogEntry();
        //    //entry.Title = "Boom Shankar Strikes back!";
        //    //entry.Content = "Let me explain this concisely:Step 1: Install and Configure IIS (required for a full " +
        //    //    "Sql Server Installation) - Details Here Step 2: Install Sql Server 2005 " +
        //    //    "a) Sql Server 2005 Express Installs without issues on Vista RC1. Sql Management Studio " +
        //    //    "Express also installs properly. Attached a couple of databases, a quick query. ";
        //    //Category cat = new Category();
        //    //cat.Id = 1;
        //    //cat.Description = "About Microsoft Sql Server";
        //    //cat.Name = "Sql Server";
        //    //entry.CategoryIds.Add(11);
        //    //entry.CategoryIds.Add(12);
        //    //entry.CategoryIds.Add(14);
        //    //new SerializationHelper().Serialize(entry, "boomshanker.txt", ProviderSettings.NamespaceURI);

        //    //DateTimeMap map = new DateTimeMap();
        //    //map.Add(DateTime.Now, Guid.NewGuid());
        //    //new SerializationHelper().Serialize(map, "boomshanker.txt");

        //    //List<Category> cats = new List<Category>();
        //    //Category cat = new Category();
        //    //cat.Id = 1;
        //    //cat.Name = "Sql Server";
        //    //cat.Description = "About Microsoft Sql Server";
        //    //cats.Add(cat);
        //    //new SerializationHelper().Serialize(cats, "boomshanker.txt", ProviderSettings.NamespaceURI);
        //}

        private void goButton_Click(object sender, EventArgs e)
        {
            switch (dataTypeComboBox.Text)
            {
                case "TemplateMap":
                    GoTemplateMap();
                    break;
                case "PingServiceList":
                    GoPingServiceList();
                    break;
                case "BlogConfig":
                    GoBlogConfig();
                    break;
            }
        }

        private void GoTemplateMap()
        {
            TemplateMap map = new TemplateMap();
            TemplateMapEntry entry = new TemplateMapEntry();
            entry.EntryId = Guid.NewGuid();
            entry.Template = "widescreen.aspx";
            map.Entries.Add(entry);
            new SerializationHelper().Serialize(map, "templatemap.xml", ProviderSettings.NamespaceURI);
        }

        private void GoPingServiceList()
        {
            PingServicesList pingsvcs = new PingServicesList();
            pingsvcs.Entries.Add("blo.gs");
            pingsvcs.Entries.Add("weblogs");
            pingsvcs.Entries.Add("technorati");
            pingsvcs.Entries.Add("google-blogsearch");
            new SerializationHelper().Serialize(pingsvcs, "pingserviceslist.xml", ProviderSettings.NamespaceURI);
        }

        private void GoBlogConfig()
        {
            BlogConfig config = new BlogConfig();
            config.Administrator = "jeswin";
            config.AlternateUrl = "http://www.process64.com";
            config.AutoSavePosts = true;
            config.BlogTitle = "Process64";
            config.Contact = "Jeswin P.";
            config.ContactEmail = "jeswin@gmail.com";
            config.Copyright = "2600 Jeswin";
            (config.Editors = new List<string>()).Add("jeswin");
            config.EnableMetaWeblogAPI = true;
            config.EnableRssHeaderAndFooter = true;
            config.Name = "process64";
            config.NumEntriesOnPage = 15;
            config.NumItemsOnFeed = 10;
            config.PingWeblogUpdateTrackers = false;
            config.RssFooter = "--- RSS FOOTER ---";
            config.RssHeader = "^^^ RSS HEADER ^^^";
            config.ShowCommentCount = true;
            config.TruncateContentInFeed = true;
            (config.UrlMapExtensions = new List<string>()).Add("aspx");
            config.UseCaptchaForComments = true;
            config.UseTitleForLinks = true;
            config.UseUrlMapping = true;
            new SerializationHelper().Serialize(config, "blog.config", ProviderSettings.NamespaceURI);
        }
    }
}